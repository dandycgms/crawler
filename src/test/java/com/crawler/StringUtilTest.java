package com.crawler;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;

import com.crawler.enums.TokenOption;
import com.crawler.tokenizer.Token;
import com.crawler.tokenizer.Tokenizer;
import com.crawler.util.StringUtil;

public class StringUtilTest {
	
	@Test
	public void cleanEscapes() {
		String strcln = StringUtil.cleanEscapes("01 a b      c\nd\n\ne\tf\t\tg \nh \ti\n\tj \n\tk \t\nl\n \tm\n\t o\t\n p");  
		assertThat(strcln, equalTo("01 a b c d ╬ e f g h i j k l m o p"));
	}

	@Test
	public void applySeparators() {
		String strcln = Tokenizer.applySeparators(".,;:\"'!?/()[]{}-+=*&%$#@", StringUtil.separators_chars);
		assertThat(strcln, equalTo(" .  ,  ;  :  \"  '  !  ?  /  (  )  [  ]  {  }  -  +  =  *  &  %  $  #  @ "));
	} 

	@Test
	public void splitInTokens() {
		List<Token> l = Tokenizer.splitInTokens("01 a b      c\nd\n\ne\tf\t\tg \nh \ti\n\tj \n\tk \t\nl\n \tm\n\t o\t\n p.,;:\"'!?/()[]{}-+=*&%$#@", TokenOption.VALUE);
		assertThat(l.toString(), equalTo("[01, a, b, c, d, ╬, e, f, g, h, i, j, k, l, m, o, p, ., ,, ;, :, \", ', !, ?, /, (, ), [, ], {, }, -, +, =, *, &, %, $, #, @]"));
	}

	@Test
	public void splitInTokensAll() {
		List<Token> l = Tokenizer.splitInTokens("01 a b      c\nd\n\ne\tf\t\tg \nh \ti\n\tj \n\tk \t\nl\n \tm\n\t o\t\n p.,;:\"'!?/()[]{}-+=*&%$#@", TokenOption.VALUE, "");
		assertThat(l.toString(), equalTo("[0, 1,  , a,  , b,  ,  ,  ,  ,  ,  , c, \n, d, \n, \n, e, \t, f, \t, \t, g,  , \n, h,  , \t, i, \n, \t, j,  , \n, \t, k,  , \t, \n, l, \n,  , \t, m, \n, \t,  , o, \t, \n,  , p, ., ,, ;, :, \", ', !, ?, /, (, ), [, ], {, }, -, +, =, *, &, %, $, #, @]"));
	}

}