package com.crawler;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;

import com.crawler.enums.TokenOption;
import com.crawler.model.Document;
import com.crawler.model.HDTW;
import com.crawler.tokenizer.Tokenizer;
import com.crawler.util.FilesUtil;

public class HDTWTest {

	@Test
	public void singleAlignment() {
		HDTW hdtw = new HDTW(TokenOption.VALUE);
		hdtw.execute("a b b a b c b c b d b c b d b c", 
					 "b b b c d c d x b x x", 
					 "c b b x x b c b c d b d b c b d b x c a c", 
					 "d a b c d x",
					 "x x b b a a b a a d d d a b d d b x c a c");
		assertThat(hdtw.toString(),equalTo("Score: 114.64393446710157\n"
				+ "Result:\n"
				+ "\t░░░d░ab░░c░░d░░░░░░░░x\n"
				+ "\t░░bb░░b░░c░░d░cdxbx░░x\n"
				+ "\t░abb░abcbc░bdbcbdb░░░c\n"
				+ "\txxbbaab░aadddabddbxcac\n"
				+ "\t░cbbxxbcbcdbdbcbdbxcac"));
	}
	
	@Test
	public void alignmentDocs() {
		List<String> listFilesForFolder = FilesUtil.listFilesForFolder("./src/main/resources/pdfs/");
		int nSamples = 5;//listFilesForFolder.size();
		String content[] = new String[nSamples];
		int i = 0;
		for (String path : listFilesForFolder) {
			if (i >= content.length)
				break;
			System.out.println("Proc. " + path);
			Document document = new Document(path).clipMargins();
			content[i++] = document.getFirstCleanPage();
System.out.println(" -> " + content[i-1].substring(0, 512));
System.out.println(" -> " + Tokenizer.splitInTokens(content[i-1], TokenOption.KIND));
			document.close();
		}
		HDTW hdtw = new HDTW(TokenOption.KIND);
		hdtw.execute(content);
		System.out.println(hdtw);
	}

}
