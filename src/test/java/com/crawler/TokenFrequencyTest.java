package com.crawler;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;

import com.crawler.enums.TokenOption;
import com.crawler.model.Document;
import com.crawler.tokenizer.Token;
import com.crawler.tokenizer.TokenFrequecy;
import com.crawler.util.FilesUtil;

public class TokenFrequencyTest {
	
	@Test
	public void tokenFrequency() {
		for (String path : FilesUtil.listFilesForFolder("./src/main/resources/pdfs/")) {
			System.out.println("Proc. " + path);
			Document document = new Document(path);
			List<Token> tokensFromFirstPage = document.getTokensFromFirstPage(TokenOption.ID);
			TokenFrequecy.addTokens(tokensFromFirstPage);
			document.close();
		}
		TokenFrequecy.show();
		assertThat(1, equalTo(1));
	}
}
