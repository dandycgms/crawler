package com.crawler;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

import org.junit.Test;

import com.crawler.xregex.XRegex;
import com.crawler.xregex.XRegexNode;

public class XRegexTest {
	
	@Test
	public void run() {
		System.out.println();
		XRegexNode r;
		r = (XRegexNode) XRegex.REGEX.extract("'\\d'+");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.DECLARE.extract("'\\d'+");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.DECLARE.extract("A1:'\\d'+");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.LOOP.extract("|'\\d'|<#123>");
		r.run("a dfkj s ask 23 32 12321ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.REGEX.extract("|'\\d'|<#123>");
		r.run("a dfkj s ask 23 32 12321ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.DECLARE.extract("A2:|'\\d'|<#123>");
		r.run("a dfkj s ask 23 32 12321ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.SET.extract("{'a','s'}"); // EQUIV (a|s) ou [as]
		r.run("a dfkj s ask 23 32 12321ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.SET.extract("{'a','s'}s<d% % %43>");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.SET.extract("{'a','s'}&");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.SET.extract("{'a','S'}i");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.SET.extract("{'dgkm','kfos'}x2");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.REGEX.extract("{'a','s'}s<d% % %43>&ix2");
		r.run("a dfkj s ask 23 32 12321ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.DECLARE.extract("A2:{'a','s'}s<d% % %43>&ix2");
		r.run("a dfkj s ask 23 32 12321ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.SEQUENCE_GROUP.extract("['a','s']");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.SEQUENCE_GROUP.extract("['a','s']s<d% % %43>");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.SEQUENCE_GROUP.extract("['a','s']&");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.SEQUENCE_GROUP.extract("['a','S']i");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.SEQUENCE_GROUP.extract("['a','d','b','m','s']x2");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.SEQUENCE_GROUP.extract("[:'a','s':]");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.DELIM_SEQ.extract("^'a','23'$");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.GROUP_REF.extract("\\1");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.REGEX.extract("['a','b']s<d% % %43>&ix2_*");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.DECLARE.extract("B:['a','b']s<d% % %43>&ix2");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.REGEX.extract("^'a','23'$_?");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.DECLARE.extract("C:^'a','23'$_?");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.OPERATION.extract("A=>'1231'");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.REGEX.extract("A=>'1231'");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.DECLARE.extract("D:A=>'1231'");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.OPERATION.extract("equals(A,B)?'\\d':'\\@'");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.REGEX.extract("equals(A,B)?'\\d':'\\@'");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();
		r = (XRegexNode) XRegex.DECLARE.extract("E:equals(A,B)?'\\d':'\\@'");
		r.run("a dfkj s ask 23 ddf 23 jfoi 43");
		System.out.println();

	}

	@Test
	public void runINTERVAL() {
		// EQUIV [A-Za-z0-9]
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~",
				"'\\@'").toString(),
				equalTo("[a, d, f, k, j, s, A, S, K, 2, 3, d, d, f, 2, 3, j, f, o, i, 4, 3]"));
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~",
				"'~\\@'").toString(),
				equalTo("[,, ., !,  , \n, \t\r,  ,  , -!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~]"));
		// EQUIV [a-z]
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~", 
				"'\\w'").toString(),
				equalTo("[a, d, f, k, j, s, d, d, f, j, f, o, i]"));
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~", 
				"'~\\w'").toString(),
				equalTo("[,, ., !ASK 23\n, \t\r23 ,  43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~]"));
		// EQUIV [A-Z]
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~", 
				"'\\W'").toString(),
				equalTo("[A, S, K]"));
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~", 
				"'~\\W'").toString(),
				equalTo("[a,dfkj.s!,  23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~]"));	
		// EQUIV [-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~]
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~",
				"'\\p'").toString(),
				equalTo("[,, ., !, -, !, \", #, $, %, &, ', (, ), *, +, ,, ., /, :, ;, <, =, >, ?, @, [, ], \\, _, `, {, |, }, ~]"));
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~",
				"'~\\p'").toString(),
				equalTo("[a, dfkj, s, ASK 23\nddf\t\r23 jfoi 43]"));
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~", 
				"'\\a'").toString(),
				equalTo("[a, d, f, k, j, s, A, S, K, d, d, f, j, f, o, i]"));
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~", 
				"'~\\a'").toString(),
				equalTo("[,, ., !,  23\n, \t\r23 ,  43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~]"));
		// EQUIV [ \t\r\n]
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~", 
				"'\\s'").toString(),
				equalTo("[ , \n, \t, \r,  ,  ]"));
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~", 
				"'~\\s'").toString(),
				equalTo("[a,dfkj.s!ASK, 23, ddf, 23, jfoi, 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~]"));
		// EQUIV [0-9]
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~", 
				"'\\d'").toString(),
				equalTo("[2, 3, 2, 3, 4, 3]"));
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~", 
				"'~\\d'").toString(),
				equalTo("[a,dfkj.s!ASK , \nddf\t\r,  jfoi , -!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~]"));
		// EQUIV [\\x20-\\x7E]
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~", 
				"'\\v'").toString(),
				equalTo("[a, ,, d, f, k, j, ., s, !, A, S, K,  , 2, 3, d, d, f, 2, 3,  , j, f, o, i,  , 4, 3, -, !, \", #, $, %, &, ', (, ), *, +, ,, ., /, :, ;, <, =, >, ?, @, [, ], \\, _, `, {, |, }, ~]"));
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~", 
				"'~\\v'").toString(),
				equalTo("[\n, \t\r]"));
		// EQUIV [\\x00-\\x1F\\x7F]
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~", 
				"'\\c'").toString(),
				equalTo("[\n, \t, \r]"));
		assertThat(XRegex.INTERVAL.run("a,dfkj.s!ASK 23\nddf\t\r23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~", 
				"'~\\c'").toString(),
				equalTo("[a,dfkj.s!ASK 23, ddf, 23 jfoi 43-!\"#$%&'()*+,./:;<=>?@[]\\_`{|}~]"));
		// EQUIV [00-99]
		assertThat(XRegex.INTERVAL.run("a dfkj s ask 23 ddf 23 jfoi 43 e 473 ou 2", "'\\d\\d'").toString(), 
				equalTo("[23, 23, 43, 47]"));
		assertThat(XRegex.INTERVAL.run("a dfkj s ask 23 ddf 23 jfoi 43 e 473 ou 2", 
				"'~\\d\\d'").toString(), 
				equalTo("[a dfkj s ask ,  ddf ,  jfoi ,  e , 3 ou 2]"));
		// EQUIV [b-z][b-z][b-z]
		assertThat(XRegex.INTERVAL.run("a dfkj s ask 23 ddf 23 jfoi 43 e 473 ou 2", "'bbb-zzz'").toString(), 
				equalTo("[dfk, ddf, jfo]"));
		assertThat(XRegex.INTERVAL.run("a dfkj s ask 23 ddf 23 jfoi 43 e 473 ou 2", 
				"'~bbb-zzz'").toString(), 
				equalTo("[a , j s ask 23 ,  23 , i 43 e 473 ou 2]"));
		// EQUIV [0-3][a-x][0-3]
		assertThat(XRegex.INTERVAL.run("a dfkj s ask 23 ddf 23 jfoi 43 e 2y3 2x2 2z3 4v2 ou 2", "'0a0-3x3'").toString(), 
				equalTo("[2x2]"));
		assertThat(XRegex.INTERVAL.run("a dfkj s ask 23 ddf 23 jfoi 43 e 2y3 2x2 2z3 4v2 ou 2", 
				"'~0a0-3x3'").toString(), 
				equalTo("[a dfkj s ask 23 ddf 23 jfoi 43 e 2y3 ,  2z3 4v2 ou 2]"));
	}
	@Test
	public void perfExtractors() {
		for (int i = 0; i < 100; i++) {
			XRegex.ALPHA_STARTED_WITH_LETTER.extract("a2hjkADn323jd&dkd");
			XRegex.QUOTED_REGEX.extract("'asdf'");
			XRegex.NUMBER.extract("3123kjn12");
			XRegex.COLISION.extract("x3123kjn12");
			XRegex.ANY_CHAR_NOTPERCENT.extract("#@hi2H#Dih%4h%");
			XRegex.ANY_CHAR_NOTPERCENT_NOTGT.extract("#@hi2H#Dih%4h%");
			XRegex.ANY_CHAR_NOTPERCENT_NOTGT.extract("#@hi2H#Dih>4h%");
			XRegex.ANY_CHAR_NOTQUOTE.extract("#@hi2H#Dih'>4h%");
			XRegex.MAND_SCHAR.extract("#@hi2H#Dih%4h%");
			XRegex.L_OPT_SCHAR.extract("#@hi2H#Dih%4h%");
			XRegex.L_OPT_SCHAR.extract("#@hi2H#Dih>4h%");
			XRegex.OPT_SCHAR.extract("#@hi2H#Dih%4h%");
			XRegex.INTERVAL_TAIL.extract("-#@hi2H#Dih'%4h%");
			XRegex.COMPOSED_OCCURRENCES.extract(",342>2332");
			XRegex.ANY_CHAR_NOTQUOTE.extract("dhi23AIdu__21'3fW");
			XRegex.INC_OCCURRENCES.extract("+>3e2");
			XRegex.SINGLE_CLOSE_OCCURRENCES.extract(">323");
			XRegex.AND.extract("&&@hi2H#Dih%4h%");
			XRegex.OR.extract("||@hi2H#Dih%4h%");
			XRegex.DELIM_SEQ_TAIL.extract("!#@hi2H#Dih%4h%");
			XRegex.VAR_N.extract("#@hi2H#Dih%4h%");
			XRegex.SPARSE.extract("&#@hi2H#Dih%4h%");
			XRegex.CASE_INSENSITIVE.extract("i#@hi2H#Dih%4h%");
			XRegex.MAND_OCCURRENCES.extract("+#@hi2H#Dih%4h%");
			XRegex.OPT_OCCURRENCES.extract("?#@hi2H#Dih%4h%");
			XRegex.OPT_REQ_OCCURRENCES.extract("*#@hi2H#Dih%4h%");
			XRegex.SEPARATOR.extract("xx");
			XRegex.SEPARATOR.extract("s<%12Ddwe%D212%>xx");
			XRegex.SEPARATOR.extract("s<12w%12Ddwe%D212%>xx");
			XRegex.SEPARATOR.extract("s<%12Ddwe%D212%2sw2>xx");
			XRegex.SEPARATOR.extract("s<21Sa%12Ddwe%D212%2sw2>xx");
			XRegex.SET_SEQ_TAIL.extract("s<%12Ddwe%D212%>w4");
			XRegex.SET_SEQ_TAIL.extract("s<%12Ddwe%D212%>&wx");
			XRegex.SET_SEQ_TAIL.extract("s<%12Ddwe%D212%>iwx");
			XRegex.SET_SEQ_TAIL.extract("s<%12Ddwe%D212%>&iwx");
			XRegex.SET_SEQ_TAIL.extract("s<%12Ddwe%D212%>&ix2wx");
			XRegex.SET_SEQ_TAIL.extract("s<%12Ddwe%D212%>i&wx");
			XRegex.SET_SEQ_TAIL.extract("&wx");
			XRegex.SET_SEQ_TAIL.extract("&iwx");
			XRegex.SET_SEQ_TAIL.extract("&ix2wx");
			XRegex.SET_SEQ_TAIL.extract("&is<%12Ddwe%D212%>wx");
			XRegex.SET_SEQ_TAIL.extract("&s<%12Ddwe%D212%>iwx");
			XRegex.SET_SEQ_TAIL.extract("ix2wxx");
			XRegex.SET_SEQ_TAIL.extract("i&wxx");
			XRegex.OCCURRENCES_COUNT.extract("23+>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES_COUNT.extract("3,43>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES_COUNT.extract("3>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES_COUNT.extract("#0392>#@hi2H#Dih%4h%");
			XRegex.VAR_OCCURRENCES_COUNT.extract("#423>e323");
			XRegex.NUM_OCCURRENCES_COUNT.extract("23+>#@hi2H#Dih%4h%");
			XRegex.NUM_OCCURRENCES_COUNT.extract("3,43>#@hi2H#Dih%4h%");
			XRegex.NUM_OCCURRENCES_COUNT.extract("3>#@hi2H#Dih%4h%");
			XRegex.LOGICAL_OP.extract("&&@hi2H#Dih%4h%");
			XRegex.LOGICAL_OP.extract("||@hi2H#Dih%4h%");
			XRegex.CLOSE_OCCURRENCES.extract(">sd");
			XRegex.CLOSE_OCCURRENCES.extract("+>sd");
			XRegex.CLOSE_OCCURRENCES.extract(",32>sd");
			XRegex.OPEN_OCCURRENCES.extract("<23+>#@hi2H#Dih%4h%");
			XRegex.OPEN_OCCURRENCES.extract("<3,43>#@hi2H#Dih%4h%");
			XRegex.OPEN_OCCURRENCES.extract("<3>#@hi2H#Dih%4h%");
			XRegex.OPEN_OCCURRENCES.extract("<#0392>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES_BODY.extract("<23+>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES_BODY.extract("<3,43>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES_BODY.extract("<3>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES_BODY.extract("<#0392>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES_BODY.extract("+#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES_BODY.extract("?#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES_BODY.extract("*#@hi2H#Dih%4h%");
			XRegex.LAZY_OCCURRENCES.extract("_<23+>#@hi2H#Dih%4h%");
			XRegex.LAZY_OCCURRENCES.extract("_<3,43>#@hi2H#Dih%4h%");
			XRegex.LAZY_OCCURRENCES.extract("_<3>#@hi2H#Dih%4h%");
			XRegex.LAZY_OCCURRENCES.extract("_<#0392>#@hi2H#Dih%4h%");
			XRegex.LAZY_OCCURRENCES.extract("_+#@hi2H#Dih%4h%");
			XRegex.LAZY_OCCURRENCES.extract("_?#@hi2H#Dih%4h%");
			XRegex.LAZY_OCCURRENCES.extract("_*#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES.extract("<23+>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES.extract("<3,43>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES.extract("<3>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES.extract("<#0392>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES.extract("+#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES.extract("?#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES.extract("*#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES.extract("_<23+>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES.extract("_<3,43>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES.extract("_<3>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES.extract("_<#0392>#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES.extract("_+#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES.extract("_?#@hi2H#Dih%4h%");
			XRegex.OCCURRENCES.extract("_*#@hi2H#Dih%4h%");
			XRegex.GROUP_REF.extract("\\3123kjn12");
			XRegex.LOOP.extract("|\\31|<#212>");
			XRegex.INTERVAL.extract("'a'");
			XRegex.INTERVAL.extract("'a-z'");
			XRegex.SET.extract("{'a','b'}");
			XRegex.DELIM_SEQ.extract("^'3-9',{'a','b'}$");
			XRegex.DELIM_SEQ.extract("^'3-9','\\@'+,'\\d'+,{'a','b'}$");
			XRegex.GROUP.extract(":'3-9':");
			XRegex.SEQUENCE_GROUP.extract("['a-z','0'+]x31");
			XRegex.SEQUENCE.extract("['a-z','0'+]x31");
			XRegex.SEQUENCE.extract("^'3-9','\\@'+,'\\d'+,{'a','b'}$");
			XRegex.SEQUENCE_BODY.extract(":'3-9':");
			XRegex.OPERATION.extract("A=>'asdf'");
			XRegex.OPERATION_TAIL.extract("=>'asdf'");
			XRegex.SUBSTITUTION.extract("=>'asdf'");
			XRegex.CONDITION.extract("('x')&&F()?'1':'32'*");
			XRegex.CONDITION_IF.extract("()");
			XRegex.FUNCT_TAIL.extract("(A)");
			XRegex.FUNCT.extract("F(A)");
			XRegex.PROCEDURE_TAIL.extract("&&F()||Ahiu43() sa");
			XRegex.CONDITION_IF.extract("('x')&&F()");
			XRegex.CONDITIONAL_THEN.extract("?'1'ds");
			XRegex.CONDITIONAL_ELSE.extract(":'32'*");
			XRegex.REGEX_BODY.extract("A=>'asdf'");
			XRegex.REGEX_BODY.extract("\\3123kjn12");
			XRegex.REGEX_BODY.extract("|\\31|<#212>");
			XRegex.REGEX_BODY.extract("'a-z'");
			XRegex.REGEX_BODY.extract("{'a','b'}");
			XRegex.REGEX_BODY.extract("['a-z','0'+]x31");
			XRegex.REGEX_BODY.extract("A('x')&&F()?'1':'32'*");
			XRegex.REGEX.extract("A=>'asdf'+");
			XRegex.REGEX.extract("\\3123*kjn12");
			XRegex.REGEX.extract("|\\31|<#212><6>");
			XRegex.REGEX.extract("'a-z'?");
			XRegex.REGEX.extract("{'a','b'}<3>");
			XRegex.REGEX.extract("['a-z','0'+]x31");
			XRegex.REGEX.extract("A('x')&&F()?'1':'32'*");
			XRegex.REGEX_LIST.extract("['a-z','0'+]x31,A('x')&&F()?'1':'32'*");
			XRegex.REGEX_LIST_OPT.extract("");
			XRegex.REGEX_LIST_OPT.extract("['a-z','0'+]x31,A('x')&&F()?'1':'32'*");
			XRegex.DECLARE.extract("X:A=>'asdf'+");
			XRegex.DECLARE.extract("X:\\3123*kjn12");
			XRegex.DECLARE.extract("X:|\\31|<#212><6>");
			XRegex.DECLARE.extract("X:'a-z'?");
			XRegex.DECLARE.extract("X:{'a','b'}<3>");
			XRegex.DECLARE.extract("X:['a-z','0'+]x31");
			XRegex.DECLARE.extract("X:A('x')&&F()?'1':'32'*");
			XRegex.DECLARE_LIST.extract("X:'a-z'?;Y:{'a','b'}<3>;Z:A=>'asdf'+;W:\\3123*kjn12");			
		}
	}
	
	@Test 
	public void extractExecutors() {
		
		assertThat(XRegex.GROUP_REF.extract("\\3123kjn12").toString(), equalTo("XRegexNode [id=GROUP_REF, param=[3123]]"));

		assertThat(XRegex.LOOP.extract("|\\31|<#212>").toString(), equalTo("XRegexNode "
				+ "["
					+ "id=LOOP, "
					+ "param=["
						+ "["
							+ "XRegexNode "
								+ "["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode "
											+ "["
												+ "id=GROUP_REF, "
												+ "param=[31]"
											+ "], {}]"
								+ "]"
						+ "], "
						+ "[212, .]"
					+ "]"
				+ "]"));
		
		assertThat(XRegex.INTERVAL.extract("'a'").toString(), equalTo("XRegexNode [id=INTERVAL, param=[true, a]]"));
		assertThat(XRegex.INTERVAL.extract("'a-z'").toString(), equalTo("XRegexNode [id=INTERVAL, param=[true, a, z]]"));
		
		assertThat(XRegex.SET.extract("{'a','b'}").toString(), equalTo("XRegexNode [id=SET, param=[true, [XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, a]], {}]], XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, b]], {}]]], {}]]"));
		
		assertThat(XRegex.DELIM_SEQ.extract("^'3-9',{'a','b'}$").toString(),
			equalTo("XRegexNode "
				+ "["
					+ "id=DELIM_SEQ, "
					+ "param=["
						+ "XRegexNode ["
							+ "id=REGEX, "
							+ "param=["
								+ "XRegexNode [id=INTERVAL, param=[true, 3, 9]], {}"
							+ "]"
						+ "], "
						+ "XRegexNode ["
							+ "id=REGEX, "
							+ "param=["
								+ "XRegexNode ["
									+ "id=SET, "
									+ "param=["
										+ "true, ["
											+ "XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, a]], {}]], "
											+ "XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, b]], {}]]"
										+ "], {}"
									+ "]"
								+ "], {}"
							+ "]"
						+ "]"
					+ "]"
				+ "]"));
		assertThat(XRegex.DELIM_SEQ.extract("^'3-9','\\@'+,'\\d'+,{'a','b'}$").toString(), 
				equalTo("XRegexNode "
					+ "["
						+ "id=DELIM_SEQ, "
						+ "param=["
							+ "XRegexNode ["
								+ "id=REGEX, "
								+ "param=["
									+ "XRegexNode [id=INTERVAL, param=[true, 3, 9]], {}"
								+ "]"
							+ "], "
							+ "XRegexNode ["
								+ "id=REGEX, "
								+ "param=["
									+ "XRegexNode [id=INTERVAL, param=[true, \\@]], {number=[1, +], type=const}]"
							+ "], "
							+ "XRegexNode ["
								+ "id=REGEX, "
								+ "param=["
									+ "XRegexNode [id=INTERVAL, param=[true, \\d]], {number=[1, +], type=const}]"
							+ "], "
							+ "XRegexNode ["
								+ "id=REGEX, "
								+ "param=["
									+ "XRegexNode ["
										+ "id=SET, "
										+ "param=["
											+ "true, ["
												+ "XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, a]], {}]], "
												+ "XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, b]], {}]]"
											+ "], {}"
										+ "]"
									+ "], {}"
								+ "]"
							+ "]"
						+ "]"
					+ "]"));
		assertThat(XRegex.GROUP.extract(":'3-9':").toString(), equalTo("XRegexNode [id=GROUP, param=[XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, 3, 9]], {}]]]]"));
		assertThat(XRegex.SEQUENCE_GROUP.extract("['a-z','0'+]x31").toString(), equalTo(
			"XRegexNode ["
					+ "id=SEQUENCE_GROUP, "
					+ "param=["
						+ "["
							+ "XRegexNode ["
								+ "id=REGEX, "
								+ "param=["
									+ "XRegexNode ["
										+ "id=INTERVAL, "
										+ "param=[true, a, z]"
									+ "], {}"
								+ "]"
							+ "], "
							+ "XRegexNode ["
								+ "id=REGEX, "
								+ "param=["
									+ "XRegexNode ["
										+ "id=INTERVAL, "
										+ "param=[true, 0]"
									+ "], "
									+ "{number=[1, +], type=const}"
								+ "]"
							+ "]"
						+ "], {sparse=false, separator={}, colision=31, icase=false}"
					+ "]"
				+ "]"));
		assertThat(XRegex.SEQUENCE.extract("['a-z','0'+]x31").toString(), equalTo(
				"XRegexNode ["
						+ "id=SEQUENCE_GROUP, "
						+ "param=["
							+ "["
								+ "XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode ["
											+ "id=INTERVAL, "
											+ "param=[true, a, z]"
										+ "], {}"
									+ "]"
								+ "], "
								+ "XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode ["
											+ "id=INTERVAL, "
											+ "param=[true, 0]"
										+ "], "
										+ "{number=[1, +], type=const}"
									+ "]"
								+ "]"
							+ "], {sparse=false, separator={}, colision=31, icase=false}"
						+ "]"
					+ "]"));
		assertThat(XRegex.SEQUENCE.extract("^'3-9','\\@'+,'\\d'+,{'a','b'}$").toString(), 
				equalTo("XRegexNode "
				+ "["
					+ "id=DELIM_SEQ, "
					+ "param=["
						+ "XRegexNode ["
							+ "id=REGEX, "
							+ "param=["
								+ "XRegexNode [id=INTERVAL, param=[true, 3, 9]], {}"
							+ "]"
						+ "], "
						+ "XRegexNode ["
							+ "id=REGEX, "
							+ "param=["
								+ "XRegexNode [id=INTERVAL, param=[true, \\@]], {number=[1, +], type=const}]"
						+ "], "
						+ "XRegexNode ["
							+ "id=REGEX, "
							+ "param=["
								+ "XRegexNode [id=INTERVAL, param=[true, \\d]], {number=[1, +], type=const}]"
						+ "], "
						+ "XRegexNode ["
							+ "id=REGEX, "
							+ "param=["
								+ "XRegexNode ["
									+ "id=SET, "
									+ "param=["
										+ "true, ["
											+ "XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, a]], {}]], "
											+ "XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, b]], {}]]"
										+ "], {}"
									+ "]"
								+ "], {}"
							+ "]"
						+ "]"
					+ "]"
				+ "]"));
		assertThat(XRegex.SEQUENCE_BODY.extract(":'3-9':").toString(), equalTo("XRegexNode [id=GROUP, param=[XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, 3, 9]], {}]]]]"));

		assertThat(XRegex.OPERATION.extract("A=>'asdf'").toString(), equalTo("XRegexNode [id=OPERATION, param=[true, A, XRegexNode [id=SUBSTITUTION, param=[asdf]]]]"));
		assertThat(XRegex.OPERATION_TAIL.extract("=>'asdf'").toString(), equalTo("XRegexNode [id=SUBSTITUTION, param=[asdf]]"));
		assertThat(XRegex.SUBSTITUTION.extract("=>'asdf'").toString(), equalTo("XRegexNode [id=SUBSTITUTION, param=[asdf]]"));
		
		assertThat(XRegex.CONDITION.extract("('x')&&F()?'1':'32'*").toString(), equalTo(
			"XRegexNode ["
				+ "id=CONDITION, "
				+ "param=["
					+ "{"
						+ "if=[XRegexNode ["
									+ "id=REGEX, "
									+ "param=[XRegexNode [id=INTERVAL, param=[true, x]], {}]"
								+ "], "
								+ "AND, "
								+ "{param=[], id=F}"
						+ "], "
						+ "else=XRegexNode ["
							+ "id=REGEX, "
							+ "param=["
								+ "XRegexNode [id=INTERVAL, param=[true, 32]], "
								+ "{number=[0, +], type=const}"
							+ "]"
						+ "], "
						+ "then=XRegexNode ["
							+ "id=REGEX, "
							+ "param=["
								+ "XRegexNode [id=INTERVAL, param=[true, 1]], "
								+ "{}"
							+ "]"
						+ "]"
					+ "}"
				+ "]"
			+ "]"));
		assertThat(XRegex.CONDITION_IF.extract("()").toString(), equalTo("{if=[]}"));
		assertThat(XRegex.FUNCT_TAIL.extract("(A)").toString(), equalTo("[XRegexNode [id=REGEX, param=[XRegexNode [id=OPERATION, param=[true, A]], {}]]]"));
		assertThat(XRegex.FUNCT.extract("F(A)").toString(),     equalTo("{param=[XRegexNode [id=REGEX, param=[XRegexNode [id=OPERATION, param=[true, A]], {}]]], id=F}"));
		assertThat(XRegex.PROCEDURE_TAIL.extract("&&F()||Ahiu43() sa").toString(), equalTo("[AND, {param=[], id=F}, OR, {param=[], id=Ahiu43}]"));
		assertThat(XRegex.CONDITION_IF.extract("('x')&&F()").toString(), equalTo(
			"{"
				+ "if=["
					+ "XRegexNode ["
						+ "id=REGEX, "
						+ "param=[XRegexNode [id=INTERVAL, param=[true, x]], {}]"
					+ "], "
					+ "AND, "
					+ "{param=[], id=F}"
				+ "]"
			+ "}"
		));
		assertThat(XRegex.CONDITIONAL_THEN.extract("?'1'ds").toString(), equalTo("{then=XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, 1]], {}]]}"));
		assertThat(XRegex.CONDITIONAL_ELSE.extract(":'32'*").toString(), equalTo("XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, 32]], {number=[0, +], type=const}]]"));

		assertThat(XRegex.REGEX_BODY.extract("A=>'asdf'").toString(), equalTo("XRegexNode [id=OPERATION, param=[true, A, XRegexNode [id=SUBSTITUTION, param=[asdf]]]]"));
		assertThat(XRegex.REGEX_BODY.extract("\\3123kjn12").toString(), equalTo("XRegexNode [id=GROUP_REF, param=[3123]]"));
		assertThat(XRegex.REGEX_BODY.extract("|\\31|<#212>").toString(), equalTo("XRegexNode "
				+ "["
					+ "id=LOOP, "
					+ "param=["
						+ "["
							+ "XRegexNode "
								+ "["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode "
											+ "["
												+ "id=GROUP_REF, "
												+ "param=[31]"
											+ "], {}]"
								+ "]"
						+ "], "
						+ "[212, .]"
					+ "]"
				+ "]"));
		assertThat(XRegex.REGEX_BODY.extract("'a-z'").toString(), equalTo("XRegexNode [id=INTERVAL, param=[true, a, z]]"));
		assertThat(XRegex.REGEX_BODY.extract("{'a','b'}").toString(), equalTo("XRegexNode [id=SET, param=[true, [XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, a]], {}]], XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, b]], {}]]], {}]]"));
		assertThat(XRegex.REGEX_BODY.extract("['a-z','0'+]x31").toString(), equalTo(
				"XRegexNode ["
						+ "id=SEQUENCE_GROUP, "
						+ "param=["
							+ "["
								+ "XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode ["
											+ "id=INTERVAL, "
											+ "param=[true, a, z]"
										+ "], {}"
									+ "]"
								+ "], "
								+ "XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode ["
											+ "id=INTERVAL, "
											+ "param=[true, 0]"
										+ "], "
										+ "{number=[1, +], type=const}"
									+ "]"
								+ "]"
							+ "], {sparse=false, separator={}, colision=31, icase=false}"
						+ "]"
					+ "]"));		
		assertThat(XRegex.REGEX_BODY.extract("A('x')&&F()?'1':'32'*").toString(), equalTo(
					"XRegexNode [id=OPERATION, param=[true, A, XRegexNode ["
						+ "id=CONDITION, "
						+ "param=["
							+ "{"
								+ "if=[XRegexNode ["
											+ "id=REGEX, "
											+ "param=[XRegexNode [id=INTERVAL, param=[true, x]], {}]"
										+ "], "
										+ "AND, "
										+ "{param=[], id=F}"
								+ "], "
								+ "else=XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode [id=INTERVAL, param=[true, 32]], "
										+ "{number=[0, +], type=const}"
									+ "]"
								+ "], "
								+ "then=XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode [id=INTERVAL, param=[true, 1]], "
										+ "{}"
									+ "]"
								+ "]"
							+ "}"
						+ "]"
					+ "]]]"));

		assertThat(XRegex.REGEX.extract("A=>'asdf'+").toString(), equalTo(""
				+ "XRegexNode [id=REGEX, param=[XRegexNode [id=OPERATION, param=[true, A, XRegexNode [id=SUBSTITUTION, param=[asdf]]]], {number=[1, +], type=const}]]"));
		assertThat(XRegex.REGEX.extract("\\3123*kjn12").toString(), equalTo(""
				+ "XRegexNode [id=REGEX, param=[XRegexNode [id=GROUP_REF, param=[3123]], {number=[0, +], type=const}]]"));
		assertThat(XRegex.REGEX.extract("|\\31|<#212><6>").toString(), equalTo(""
				+ "XRegexNode [id=REGEX, param=[XRegexNode ["
					+ "id=LOOP, "
					+ "param=["
						+ "["
							+ "XRegexNode "
								+ "["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode "
											+ "["
												+ "id=GROUP_REF, "
												+ "param=[31]"
											+ "], {}]"
								+ "]"
						+ "], "
						+ "[212, .]"
					+ "]"
				+ "], {number=[6, .], type=const}]]"));
		assertThat(XRegex.REGEX.extract("'a-z'?").toString(), equalTo("XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, a, z]], {number=[0, 1], type=const}]]"));
		assertThat(XRegex.REGEX.extract("{'a','b'}<3>").toString(), equalTo("XRegexNode [id=REGEX, param=[XRegexNode [id=SET, param=[true, [XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, a]], {}]], XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, b]], {}]]], {}]], {number=[3, .], type=const}]]"));
		assertThat(XRegex.REGEX.extract("['a-z','0'+]x31").toString(), equalTo(
				"XRegexNode [id=REGEX, param=[XRegexNode ["
						+ "id=SEQUENCE_GROUP, "
						+ "param=["
							+ "["
								+ "XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode ["
											+ "id=INTERVAL, "
											+ "param=[true, a, z]"
										+ "], {}"
									+ "]"
								+ "], "
								+ "XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode ["
											+ "id=INTERVAL, "
											+ "param=[true, 0]"
										+ "], "
										+ "{number=[1, +], type=const}"
									+ "]"
								+ "]"
							+ "], {sparse=false, separator={}, colision=31, icase=false}"
						+ "]"
					+ "], {}]]"));		
		assertThat(XRegex.REGEX.extract("A('x')&&F()?'1':'32'*").toString(), equalTo(
					"XRegexNode [id=REGEX, param=[XRegexNode [id=OPERATION, param=[true, A, XRegexNode ["
						+ "id=CONDITION, "
						+ "param=["
							+ "{"
								+ "if=[XRegexNode ["
											+ "id=REGEX, "
											+ "param=[XRegexNode [id=INTERVAL, param=[true, x]], {}]"
										+ "], "
										+ "AND, "
										+ "{param=[], id=F}"
								+ "], "
								+ "else=XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode [id=INTERVAL, param=[true, 32]], "
										+ "{number=[0, +], type=const}"
									+ "]"
								+ "], "
								+ "then=XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode [id=INTERVAL, param=[true, 1]], "
										+ "{}"
									+ "]"
								+ "]"
							+ "}"
						+ "]"
					+ "]]], {}]]"));
		assertThat(XRegex.REGEX_LIST.extract("['a-z','0'+]x31,A('x')&&F()?'1':'32'*").toString(), equalTo(
				"[XRegexNode [id=REGEX, param=[XRegexNode ["
						+ "id=SEQUENCE_GROUP, "
						+ "param=["
							+ "["
								+ "XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode ["
											+ "id=INTERVAL, "
											+ "param=[true, a, z]"
										+ "], {}"
									+ "]"
								+ "], "
								+ "XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode ["
											+ "id=INTERVAL, "
											+ "param=[true, 0]"
										+ "], "
										+ "{number=[1, +], type=const}"
									+ "]"
								+ "]"
							+ "], {sparse=false, separator={}, colision=31, icase=false}"
						+ "]"
					+ "], {}]], "
				+ "XRegexNode [id=REGEX, param=[XRegexNode [id=OPERATION, param=[true, A, XRegexNode ["
				+ "id=CONDITION, "
				+ "param=["
					+ "{"
						+ "if=[XRegexNode ["
									+ "id=REGEX, "
									+ "param=[XRegexNode [id=INTERVAL, param=[true, x]], {}]"
								+ "], "
								+ "AND, "
								+ "{param=[], id=F}"
						+ "], "
						+ "else=XRegexNode ["
							+ "id=REGEX, "
							+ "param=["
								+ "XRegexNode [id=INTERVAL, param=[true, 32]], "
								+ "{number=[0, +], type=const}"
							+ "]"
						+ "], "
						+ "then=XRegexNode ["
							+ "id=REGEX, "
							+ "param=["
								+ "XRegexNode [id=INTERVAL, param=[true, 1]], "
								+ "{}"
							+ "]"
						+ "]"
					+ "}"
				+ "]"
			+ "]]], {}]]]"));
		assertThat(XRegex.REGEX_LIST_OPT.extract("").toString(), equalTo("[]"));
		assertThat(XRegex.REGEX_LIST_OPT.extract("['a-z','0'+]x31,A('x')&&F()?'1':'32'*").toString(), equalTo(
				"[XRegexNode [id=REGEX, param=[XRegexNode ["
						+ "id=SEQUENCE_GROUP, "
						+ "param=["
							+ "["
								+ "XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode ["
											+ "id=INTERVAL, "
											+ "param=[true, a, z]"
										+ "], {}"
									+ "]"
								+ "], "
								+ "XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode ["
											+ "id=INTERVAL, "
											+ "param=[true, 0]"
										+ "], "
										+ "{number=[1, +], type=const}"
									+ "]"
								+ "]"
							+ "], {sparse=false, separator={}, colision=31, icase=false}"
						+ "]"
					+ "], {}]], "
				+ "XRegexNode [id=REGEX, param=[XRegexNode [id=OPERATION, param=[true, A, XRegexNode ["
				+ "id=CONDITION, "
				+ "param=["
					+ "{"
						+ "if=[XRegexNode ["
									+ "id=REGEX, "
									+ "param=[XRegexNode [id=INTERVAL, param=[true, x]], {}]"
								+ "], "
								+ "AND, "
								+ "{param=[], id=F}"
						+ "], "
						+ "else=XRegexNode ["
							+ "id=REGEX, "
							+ "param=["
								+ "XRegexNode [id=INTERVAL, param=[true, 32]], "
								+ "{number=[0, +], type=const}"
							+ "]"
						+ "], "
						+ "then=XRegexNode ["
							+ "id=REGEX, "
							+ "param=["
								+ "XRegexNode [id=INTERVAL, param=[true, 1]], "
								+ "{}"
							+ "]"
						+ "]"
					+ "}"
				+ "]"
			+ "]]], {}]]]"));

		assertThat(XRegex.DECLARE.extract("X:A=>'asdf'+").toString(), equalTo(""
				+ "XRegexNode [id=DECLARE, param=[X, XRegexNode [id=REGEX, param=[XRegexNode [id=OPERATION, param=[true, A, XRegexNode [id=SUBSTITUTION, param=[asdf]]]], {number=[1, +], type=const}]]]]"));
		assertThat(XRegex.DECLARE.extract("X:\\3123*kjn12").toString(), equalTo(""
				+ "XRegexNode [id=DECLARE, param=[X, XRegexNode [id=REGEX, param=[XRegexNode [id=GROUP_REF, param=[3123]], {number=[0, +], type=const}]]]]"));
		assertThat(XRegex.DECLARE.extract("X:|\\31|<#212><6>").toString(), equalTo(""
				+ "XRegexNode [id=DECLARE, param=[X, XRegexNode [id=REGEX, param=[XRegexNode ["
					+ "id=LOOP, "
					+ "param=["
						+ "["
							+ "XRegexNode "
								+ "["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode "
											+ "["
												+ "id=GROUP_REF, "
												+ "param=[31]"
											+ "], {}]"
								+ "]"
						+ "], "
						+ "[212, .]"
					+ "]"
				+ "], {number=[6, .], type=const}]]]]"));
		assertThat(XRegex.DECLARE.extract("X:'a-z'?").toString(), equalTo("XRegexNode [id=DECLARE, param=[X, XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, a, z]], {number=[0, 1], type=const}]]]]"));
		assertThat(XRegex.DECLARE.extract("X:{'a','b'}<3>").toString(), equalTo("XRegexNode [id=DECLARE, param=[X, XRegexNode [id=REGEX, param=[XRegexNode [id=SET, param=[true, [XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, a]], {}]], XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, b]], {}]]], {}]], {number=[3, .], type=const}]]]]"));
		assertThat(XRegex.DECLARE.extract("X:['a-z','0'+]x31").toString(), equalTo(
				"XRegexNode [id=DECLARE, param=[X, XRegexNode [id=REGEX, param=[XRegexNode ["
						+ "id=SEQUENCE_GROUP, "
						+ "param=["
							+ "["
								+ "XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode ["
											+ "id=INTERVAL, "
											+ "param=[true, a, z]"
										+ "], {}"
									+ "]"
								+ "], "
								+ "XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode ["
											+ "id=INTERVAL, "
											+ "param=[true, 0]"
										+ "], "
										+ "{number=[1, +], type=const}"
									+ "]"
								+ "]"
							+ "], {sparse=false, separator={}, colision=31, icase=false}"
						+ "]"
					+ "], {}]]]]"));		
		assertThat(XRegex.DECLARE.extract("X:A('x')&&F()?'1':'32'*").toString(), equalTo(
					"XRegexNode [id=DECLARE, param=[X, XRegexNode [id=REGEX, param=[XRegexNode [id=OPERATION, param=[true, A, XRegexNode ["
						+ "id=CONDITION, "
						+ "param=["
							+ "{"
								+ "if=[XRegexNode ["
											+ "id=REGEX, "
											+ "param=[XRegexNode [id=INTERVAL, param=[true, x]], {}]"
										+ "], "
										+ "AND, "
										+ "{param=[], id=F}"
								+ "], "
								+ "else=XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode [id=INTERVAL, param=[true, 32]], "
										+ "{number=[0, +], type=const}"
									+ "]"
								+ "], "
								+ "then=XRegexNode ["
									+ "id=REGEX, "
									+ "param=["
										+ "XRegexNode [id=INTERVAL, param=[true, 1]], "
										+ "{}"
									+ "]"
								+ "]"
							+ "}"
						+ "]"
					+ "]]], {}]]]]"));
		assertThat(XRegex.DECLARE_LIST.extract("X:'a-z'?;Y:{'a','b'}<3>;Z:A=>'asdf'+;W:\\3123*kjn12").toString(), equalTo(""
				+ "[XRegexNode [id=DECLARE, param=[X, XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, a, z]], {number=[0, 1], type=const}]]]], "
				+ "XRegexNode [id=DECLARE, param=[Y, XRegexNode [id=REGEX, param=[XRegexNode [id=SET, param=[true, [XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, a]], {}]], XRegexNode [id=REGEX, param=[XRegexNode [id=INTERVAL, param=[true, b]], {}]]], {}]], {number=[3, .], type=const}]]]], "
				+ "XRegexNode [id=DECLARE, param=[Z, XRegexNode [id=REGEX, param=[XRegexNode [id=OPERATION, param=[true, A, XRegexNode [id=SUBSTITUTION, param=[asdf]]]], {number=[1, +], type=const}]]]], "
				+ "XRegexNode [id=DECLARE, param=[W, XRegexNode [id=REGEX, param=[XRegexNode [id=GROUP_REF, param=[3123]], {number=[0, +], type=const}]]]]]"));
	}
	
	@Test 
	public void extractParams() {
		assertThat(XRegex.ALPHA_STARTED_WITH_LETTER.extract("a2hjkADn323jd&dkd"), equalTo("a2hjkADn323jd"));
		assertThat(XRegex.QUOTED_REGEX.extract("'asdf'"), equalTo("asdf"));
		assertThat(XRegex.NUMBER.extract("3123kjn12"), equalTo("3123"));
		assertThat(XRegex.COLISION.extract("x3123kjn12"), equalTo("3123"));
		assertThat(XRegex.ANY_CHAR_NOTPERCENT.extract("#@hi2H#Dih%4h%"), equalTo("#@hi2H#Dih"));
		assertThat(XRegex.ANY_CHAR_NOTPERCENT_NOTGT.extract("#@hi2H#Dih%4h%"), equalTo("#@hi2H#Dih"));
		assertThat(XRegex.ANY_CHAR_NOTPERCENT_NOTGT.extract("#@hi2H#Dih>4h%"), equalTo("#@hi2H#Dih"));
		assertThat(XRegex.ANY_CHAR_NOTQUOTE.extract("#@hi2H#Dih'>4h%"), equalTo("#@hi2H#Dih"));
		assertThat(XRegex.MAND_SCHAR.extract("#@hi2H#Dih%4h%"), equalTo("#@hi2H#Dih"));
		assertThat(XRegex.L_OPT_SCHAR.extract("#@hi2H#Dih%4h%"), equalTo("#@hi2H#Dih"));
		assertThat(XRegex.L_OPT_SCHAR.extract("#@hi2H#Dih>4h%"), equalTo("#@hi2H#Dih"));
		assertThat(XRegex.OPT_SCHAR.extract("#@hi2H#Dih%4h%"), equalTo("#@hi2H#Dih"));
		assertThat(XRegex.INTERVAL_TAIL.extract("-#@hi2H#Dih'%4h%"), equalTo("#@hi2H#Dih"));
		assertThat(XRegex.COMPOSED_OCCURRENCES.extract(",342>2332"), equalTo("342"));
		assertThat(XRegex.ANY_CHAR_NOTQUOTE.extract("dhi23AIdu__21'3fW"), equalTo("dhi23AIdu__21"));
		assertThat(XRegex.INC_OCCURRENCES.extract("+>3e2"), equalTo("+"));
		assertThat(XRegex.SINGLE_CLOSE_OCCURRENCES.extract(">323"), equalTo("."));
		assertThat(XRegex.AND.extract("&&@hi2H#Dih%4h%"), equalTo("AND"));
		assertThat(XRegex.OR.extract("||@hi2H#Dih%4h%"), equalTo("OR"));

		assertTrue((Boolean)XRegex.DELIM_SEQ_TAIL.extract("!#@hi2H#Dih%4h%"));
		assertTrue((Boolean)XRegex.VAR_N.extract("#@hi2H#Dih%4h%"));
		assertTrue((Boolean)XRegex.SPARSE.extract("&#@hi2H#Dih%4h%"));
		assertTrue((Boolean)XRegex.CASE_INSENSITIVE.extract("i#@hi2H#Dih%4h%"));
		assertFalse((Boolean)XRegex.EXCEPT.extract("~#@hi2H#Dih%4h%"));

		assertThat(XRegex.MAND_OCCURRENCES.extract("+#@hi2H#Dih%4h%").toString(), equalTo("{number=[1, +], type=const}"));
		assertThat(XRegex.OPT_OCCURRENCES.extract("?#@hi2H#Dih%4h%").toString(), equalTo("{number=[0, 1], type=const}"));
		assertThat(XRegex.OPT_REQ_OCCURRENCES.extract("*#@hi2H#Dih%4h%").toString(), equalTo("{number=[0, +], type=const}"));
		assertThat(XRegex.SEPARATOR.extract("xx").toString(), equalTo("{}"));
		assertThat(XRegex.SEPARATOR.extract("s<%12Ddwe%D212%>xx").toString(), equalTo("{p=D212, s=12Ddwe, f=, l=}"));
		assertThat(XRegex.SEPARATOR.extract("s<12w%12Ddwe%D212%>xx").toString(), equalTo("{p=D212, s=12Ddwe, f=12w, l=}"));
		assertThat(XRegex.SEPARATOR.extract("s<%12Ddwe%D212%2sw2>xx").toString(), equalTo("{p=D212, s=12Ddwe, f=, l=2sw2}"));
		assertThat(XRegex.SEPARATOR.extract("s<21Sa%12Ddwe%D212%2sw2>xx").toString(), equalTo("{p=D212, s=12Ddwe, f=21Sa, l=2sw2}"));
		assertThat(XRegex.SET_SEQ_TAIL.extract("s<%12Ddwe%D212%>wxx").toString(), equalTo("{sparse=false, separator={p=D212, s=12Ddwe, f=, l=}, colision=null, icase=false}"));
		assertThat(XRegex.SET_SEQ_TAIL.extract("s<%12Ddwe%D212%>&wxx").toString(), equalTo("{sparse=true, separator={p=D212, s=12Ddwe, f=, l=}, colision=null, icase=false}"));
		assertThat(XRegex.SET_SEQ_TAIL.extract("s<%12Ddwe%D212%>iwxx").toString(), equalTo("{sparse=false, separator={p=D212, s=12Ddwe, f=, l=}, colision=null, icase=true}"));
		assertThat(XRegex.SET_SEQ_TAIL.extract("s<%12Ddwe%D212%>&iwxx").toString(), equalTo("{sparse=true, separator={p=D212, s=12Ddwe, f=, l=}, colision=null, icase=true}"));
		assertThat(XRegex.SET_SEQ_TAIL.extract("s<%12Ddwe%D212%>i&wxx").toString(), equalTo("{sparse=false, separator={p=D212, s=12Ddwe, f=, l=}, colision=null, icase=true}"));
		assertThat(XRegex.SET_SEQ_TAIL.extract("&wxx").toString(), equalTo("{sparse=true, separator={}, colision=null, icase=false}"));
		assertThat(XRegex.SET_SEQ_TAIL.extract("&iwxx").toString(), equalTo("{sparse=true, separator={}, colision=null, icase=true}"));
		assertThat(XRegex.SET_SEQ_TAIL.extract("&is<%12Ddwe%D212%>wxx").toString(), equalTo("{sparse=true, separator={}, colision=null, icase=true}"));
		assertThat(XRegex.SET_SEQ_TAIL.extract("&s<%12Ddwe%D212%>iwxx").toString(), equalTo("{sparse=true, separator={}, colision=null, icase=false}"));
		assertThat(XRegex.SET_SEQ_TAIL.extract("iwxx").toString(), equalTo("{sparse=false, separator={}, colision=null, icase=true}"));
		assertThat(XRegex.SET_SEQ_TAIL.extract("i&wxx").toString(), equalTo("{sparse=false, separator={}, colision=null, icase=true}"));
		assertThat(XRegex.SET_SEQ_TAIL.extract("&ix2wxx").toString(), equalTo("{sparse=true, separator={}, colision=2, icase=true}"));
		assertThat(XRegex.OCCURRENCES_COUNT.extract("23+>#@hi2H#Dih%4h%").toString(), equalTo("{number=[23, +], type=const}"));
		assertThat(XRegex.OCCURRENCES_COUNT.extract("3,43>#@hi2H#Dih%4h%").toString(), equalTo("{number=[3, 43], type=const}"));
		assertThat(XRegex.OCCURRENCES_COUNT.extract("3>#@hi2H#Dih%4h%").toString(), equalTo("{number=[3, .], type=const}"));
		assertThat(XRegex.OCCURRENCES_COUNT.extract("#0392>#@hi2H#Dih%4h%").toString(), equalTo("{number=[0392, .], type=var}"));
		
		assertThat(XRegex.VAR_OCCURRENCES_COUNT.extract("#423>e323").toString(), equalTo("[423, .]"));
		assertThat(XRegex.NUM_OCCURRENCES_COUNT.extract("23+>#@hi2H#Dih%4h%").toString(), equalTo("[23, +]"));
		assertThat(XRegex.NUM_OCCURRENCES_COUNT.extract("3,43>#@hi2H#Dih%4h%").toString(), equalTo("[3, 43]"));
		assertThat(XRegex.NUM_OCCURRENCES_COUNT.extract("3>#@hi2H#Dih%4h%").toString(), equalTo("[3, .]"));

		assertThat(XRegex.LOGICAL_OP.extract("&&@hi2H#Dih%4h%"), equalTo("AND"));
		assertThat(XRegex.LOGICAL_OP.extract("||@hi2H#Dih%4h%"), equalTo("OR"));
		assertThat(XRegex.CLOSE_OCCURRENCES.extract(">sd"), equalTo("."));
		assertThat(XRegex.CLOSE_OCCURRENCES.extract("+>sd"), equalTo("+"));
		assertThat(XRegex.CLOSE_OCCURRENCES.extract(",32>sd"), equalTo("32"));
		assertThat(XRegex.OPEN_OCCURRENCES.extract("<23+>#@hi2H#Dih%4h%").toString(), equalTo("{number=[23, +], type=const}"));
		assertThat(XRegex.OPEN_OCCURRENCES.extract("<3,43>#@hi2H#Dih%4h%").toString(), equalTo("{number=[3, 43], type=const}"));
		assertThat(XRegex.OPEN_OCCURRENCES.extract("<3>#@hi2H#Dih%4h%").toString(), equalTo("{number=[3, .], type=const}"));
		assertThat(XRegex.OPEN_OCCURRENCES.extract("<#0392>#@hi2H#Dih%4h%").toString(), equalTo("{number=[0392, .], type=var}"));
		assertThat(XRegex.OCCURRENCES_BODY.extract("<23+>#@hi2H#Dih%4h%").toString(), equalTo("{number=[23, +], type=const}"));
		assertThat(XRegex.OCCURRENCES_BODY.extract("<3,43>#@hi2H#Dih%4h%").toString(), equalTo("{number=[3, 43], type=const}"));
		assertThat(XRegex.OCCURRENCES_BODY.extract("<3>#@hi2H#Dih%4h%").toString(), equalTo("{number=[3, .], type=const}"));
		assertThat(XRegex.OCCURRENCES_BODY.extract("<#0392>#@hi2H#Dih%4h%").toString(), equalTo("{number=[0392, .], type=var}"));
		assertThat(XRegex.OCCURRENCES_BODY.extract("+#@hi2H#Dih%4h%").toString(), equalTo("{number=[1, +], type=const}"));
		assertThat(XRegex.OCCURRENCES_BODY.extract("?#@hi2H#Dih%4h%").toString(), equalTo("{number=[0, 1], type=const}"));
		assertThat(XRegex.OCCURRENCES_BODY.extract("*#@hi2H#Dih%4h%").toString(), equalTo("{number=[0, +], type=const}"));
		assertThat(XRegex.LAZY_OCCURRENCES.extract("_<23+>#@hi2H#Dih%4h%").toString(), equalTo("{number=[23, +], lazy=true, type=const}"));
		assertThat(XRegex.LAZY_OCCURRENCES.extract("_<3,43>#@hi2H#Dih%4h%").toString(), equalTo("{number=[3, 43], lazy=true, type=const}"));
		assertThat(XRegex.LAZY_OCCURRENCES.extract("_<3>#@hi2H#Dih%4h%").toString(), equalTo("{number=[3, .], lazy=true, type=const}"));
		assertThat(XRegex.LAZY_OCCURRENCES.extract("_<#0392>#@hi2H#Dih%4h%").toString(), equalTo("{number=[0392, .], lazy=true, type=var}"));
		assertThat(XRegex.LAZY_OCCURRENCES.extract("_+#@hi2H#Dih%4h%").toString(), equalTo("{number=[1, +], lazy=true, type=const}"));
		assertThat(XRegex.LAZY_OCCURRENCES.extract("_?#@hi2H#Dih%4h%").toString(), equalTo("{number=[0, 1], lazy=true, type=const}"));
		assertThat(XRegex.LAZY_OCCURRENCES.extract("_*#@hi2H#Dih%4h%").toString(), equalTo("{number=[0, +], lazy=true, type=const}"));
		assertThat(XRegex.OCCURRENCES.extract("<23+>#@hi2H#Dih%4h%").toString(), equalTo("{number=[23, +], type=const}"));
		assertThat(XRegex.OCCURRENCES.extract("<3,43>#@hi2H#Dih%4h%").toString(), equalTo("{number=[3, 43], type=const}"));
		assertThat(XRegex.OCCURRENCES.extract("<3>#@hi2H#Dih%4h%").toString(), equalTo("{number=[3, .], type=const}"));
		assertThat(XRegex.OCCURRENCES.extract("<#0392>#@hi2H#Dih%4h%").toString(), equalTo("{number=[0392, .], type=var}"));
		assertThat(XRegex.OCCURRENCES.extract("+#@hi2H#Dih%4h%").toString(), equalTo("{number=[1, +], type=const}"));
		assertThat(XRegex.OCCURRENCES.extract("?#@hi2H#Dih%4h%").toString(), equalTo("{number=[0, 1], type=const}"));
		assertThat(XRegex.OCCURRENCES.extract("*#@hi2H#Dih%4h%").toString(), equalTo("{number=[0, +], type=const}"));
		assertThat(XRegex.OCCURRENCES.extract("_<23+>#@hi2H#Dih%4h%").toString(), equalTo("{number=[23, +], lazy=true, type=const}"));
		assertThat(XRegex.OCCURRENCES.extract("_<3,43>#@hi2H#Dih%4h%").toString(), equalTo("{number=[3, 43], lazy=true, type=const}"));
		assertThat(XRegex.OCCURRENCES.extract("_<3>#@hi2H#Dih%4h%").toString(), equalTo("{number=[3, .], lazy=true, type=const}"));
		assertThat(XRegex.OCCURRENCES.extract("_<#0392>#@hi2H#Dih%4h%").toString(), equalTo("{number=[0392, .], lazy=true, type=var}"));
		assertThat(XRegex.OCCURRENCES.extract("_+#@hi2H#Dih%4h%").toString(), equalTo("{number=[1, +], lazy=true, type=const}"));
		assertThat(XRegex.OCCURRENCES.extract("_?#@hi2H#Dih%4h%").toString(), equalTo("{number=[0, 1], lazy=true, type=const}"));
		assertThat(XRegex.OCCURRENCES.extract("_*#@hi2H#Dih%4h%").toString(), equalTo("{number=[0, +], lazy=true, type=const}"));
	}
	
	@Test
	public void startsPERF() {
		for (int i = 0; i < 100; i++) {
			XRegex.DECLARE_LIST_TAIL.starts(";abcbdd1");
			XRegex.DECLARE_LIST_TAIL.starts("");
			XRegex.ALPHA_STARTED_WITH_LETTER.starts("abcbdd1");
			XRegex.ALPHA_STARTED_WITH_LETTER.starts("1abcbdd1");
			XRegex.ALPHA_STARTED_WITH_LETTER.starts("S1_abcbdd1");	
			XRegex.ALPHA_STARTED_WITH_LETTER_TAIL.starts("abcbdd1");
			XRegex.ALPHA_STARTED_WITH_LETTER_TAIL.starts("1abcbdd1");
			XRegex.ALPHA_STARTED_WITH_LETTER_TAIL.starts("_abcbdd1");
			XRegex.ALPHA_STARTED_WITH_LETTER_TAIL.starts("S1_abcbdd1");
			XRegex.ALPHA_STARTED_WITH_LETTER_TAIL.starts("-abcbdd1");
			XRegex.QUOTED_REGEX.starts("\"S1_abcbdd1");
			XRegex.QUOTED_REGEX.starts("S1_abcbdd1");
			XRegex.SET.starts("{S1_abcbdd1");
			XRegex.SET.starts("S1_abcbdd1");
			XRegex.SET_SEQ_TAIL.starts("<S1_abcbdd1");
			XRegex.SET_SEQ_TAIL.starts("iS1_abcbdd1");
			XRegex.SET_SEQ_TAIL.starts("&S1_abcbdd1");
			XRegex.SET_SEQ_TAIL.starts("S1_abcbdd1");
			XRegex.GROUP_REF.starts("\\2S1_abcbdd1");
			XRegex.GROUP_REF.starts("S1_abcbdd1");
			XRegex.NUMBER.starts("0S1_abcbdd1");
			XRegex.NUMBER.starts("2S1_abcbdd1");
			XRegex.NUMBER.starts("9S1_abcbdd1");
			XRegex.NUMBER.starts("\\2S1_abcbdd1");
			XRegex.SEQUENCE.starts("^S1_abcbdd1");
			XRegex.SEQUENCE.starts("[S1_abcbdd1");
			XRegex.SEQUENCE.starts("{S1_abcbdd1");
			XRegex.SEQUENCE.starts("S1_abcbdd1");
			XRegex.LOOP.starts("|S1_abcbdd1");
			XRegex.LOOP.starts("S1_abcbdd1");
			XRegex.INTERVAL_TAIL.starts("-S1_abcbdd1");
			XRegex.INTERVAL_TAIL.starts("S1_abcbdd1");
			XRegex.ANY_CHAR_NOTQUOTE.starts("S1_abcbdd1");
			XRegex.ANY_CHAR_NOTQUOTE.starts("\"S1_abcbdd1");
			XRegex.ANY_CHAR_NOTQUOTE.starts("a\"S1_abcbdd1");
			XRegex.ANY_CHAR_NOTPERCENT.starts("S1_abcbdd1");
			XRegex.ANY_CHAR_NOTPERCENT.starts("%S1_abcbdd1");
			XRegex.ANY_CHAR_NOTPERCENT.starts("a%S1_abcbdd1");
			XRegex.REGEX_LIST_TAIL.starts(",S1_abcbdd1");
			XRegex.REGEX_LIST_TAIL.starts("S1_abcbdd1");
			XRegex.DELIM_SEQ.starts("^S1_abcbdd1");
			XRegex.DELIM_SEQ.starts("S1_abcbdd1");
			XRegex.DELIM_SEQ_TAIL.starts("!S1_abcbdd1");
			XRegex.DELIM_SEQ_TAIL.starts("S1_abcbdd1");
			XRegex.COLISION.starts("xS1_abcbdd1");
			XRegex.COLISION.starts("AxS1_abcbdd1");
			XRegex.GROUP.starts(":S1_abcbdd1");
			XRegex.GROUP.starts("AxS1_abcbdd1");
			XRegex.VAR_N.starts("#S1_abcbdd1");
			XRegex.VAR_N.starts("AxS1_abcbdd1");
			XRegex.FUNCT_TAIL.starts("(S1_abcbdd1");
			XRegex.FUNCT_TAIL.starts("AxS1_abcbdd1");
			XRegex.AND.starts("&S1_abcbdd1");
			XRegex.AND.starts("AxS1_abcbdd1");
			XRegex.OR.starts("|S1_abcbdd1");
			XRegex.OR.starts("AxS1_abcbdd1");
			XRegex.CONDITIONAL_THEN.starts("?S1_abcbdd1");
			XRegex.CONDITIONAL_THEN.starts("AxS1_abcbdd1");
			XRegex.CONDITIONAL_ELSE.starts(":S1_abcbdd1");
			XRegex.CONDITIONAL_ELSE.starts("AxS1_abcbdd1");
			XRegex.SUBSTITUTION.starts("=>S1_abcbdd1");
			XRegex.SUBSTITUTION.starts("=S1_abcbdd1");
			XRegex.SUBSTITUTION.starts("AxS1_abcbdd1");
			XRegex.LAZY_OCCURRENCES.starts("_S1_abcbdd1");
			XRegex.LAZY_OCCURRENCES.starts("AxS1_abcbdd1");
			XRegex.OCCURRENCES_BODY.starts("<S1_abcbdd1");
			XRegex.OCCURRENCES_BODY.starts("AxS1_abcbdd1");
			XRegex.SINGLE_CLOSE_OCCURRENCES.starts(">S1_abcbdd1");
			XRegex.SINGLE_CLOSE_OCCURRENCES.starts("AxS1_abcbdd1");
			XRegex.COMPOSED_OCCURRENCES.starts(",S1_abcbdd1");
			XRegex.COMPOSED_OCCURRENCES.starts("AxS1_abcbdd1");
			XRegex.INC_OCCURRENCES.starts("+S1_abcbdd1");
			XRegex.INC_OCCURRENCES.starts("AxS1_abcbdd1");
		}
	}
	
	@Test
	public void starts() {
		assertThat(XRegex.DECLARE_LIST_TAIL.starts(";abcbdd1"), equalTo(true));
		assertThat(XRegex.DECLARE_LIST_TAIL.starts(""), equalTo(false));
		assertThat(XRegex.ALPHA_STARTED_WITH_LETTER.starts("abcbdd1"), equalTo(true));
		assertThat(XRegex.ALPHA_STARTED_WITH_LETTER.starts("1abcbdd1"), equalTo(false));
		assertThat(XRegex.ALPHA_STARTED_WITH_LETTER.starts("S1_abcbdd1"), equalTo(true));	
		assertThat(XRegex.ALPHA_STARTED_WITH_LETTER_TAIL.starts("abcbdd1"), equalTo(true));
		assertThat(XRegex.ALPHA_STARTED_WITH_LETTER_TAIL.starts("1abcbdd1"), equalTo(true));
		assertThat(XRegex.ALPHA_STARTED_WITH_LETTER_TAIL.starts("_abcbdd1"), equalTo(true));
		assertThat(XRegex.ALPHA_STARTED_WITH_LETTER_TAIL.starts("S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.ALPHA_STARTED_WITH_LETTER_TAIL.starts("-abcbdd1"), equalTo(false));
		assertThat(XRegex.QUOTED_REGEX.starts("'S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.QUOTED_REGEX.starts("S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.SET.starts("{S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.SET.starts("S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.SET_SEQ_TAIL.starts("s<S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.SET_SEQ_TAIL.starts("iS1_abcbdd1"), equalTo(true));
		assertThat(XRegex.SET_SEQ_TAIL.starts("&S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.SET_SEQ_TAIL.starts("S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.GROUP_REF.starts("\\2S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.GROUP_REF.starts("S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.NUMBER.starts("0S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.NUMBER.starts("2S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.NUMBER.starts("9S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.NUMBER.starts("\\2S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.SEQUENCE.starts("^S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.SEQUENCE.starts("[S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.SEQUENCE.starts("{S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.SEQUENCE.starts("S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.LOOP.starts("|S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.LOOP.starts("S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.INTERVAL_TAIL.starts("-S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.INTERVAL_TAIL.starts("S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.ANY_CHAR_NOTQUOTE.starts("S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.ANY_CHAR_NOTQUOTE.starts("'S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.ANY_CHAR_NOTQUOTE.starts("a'S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.ANY_CHAR_NOTPERCENT.starts("S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.ANY_CHAR_NOTPERCENT.starts("%S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.ANY_CHAR_NOTPERCENT.starts("a%S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.REGEX_LIST_TAIL.starts(",S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.REGEX_LIST_TAIL.starts("S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.DELIM_SEQ.starts("^S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.DELIM_SEQ.starts("S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.DELIM_SEQ_TAIL.starts("!S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.DELIM_SEQ_TAIL.starts("S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.COLISION.starts("xS1_abcbdd1"), equalTo(true));
		assertThat(XRegex.COLISION.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.GROUP.starts(":S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.GROUP.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.VAR_N.starts("#S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.VAR_N.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.FUNCT_TAIL.starts("(S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.FUNCT_TAIL.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.AND.starts("&S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.AND.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.OR.starts("|S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.OR.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.CONDITIONAL_THEN.starts("?S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.CONDITIONAL_THEN.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.CONDITIONAL_ELSE.starts(":S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.CONDITIONAL_ELSE.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.SUBSTITUTION.starts("=>S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.SUBSTITUTION.starts("=S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.SUBSTITUTION.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.LAZY_OCCURRENCES.starts("_S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.LAZY_OCCURRENCES.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.OCCURRENCES_BODY.starts("<S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.OCCURRENCES_BODY.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.SINGLE_CLOSE_OCCURRENCES.starts(">S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.SINGLE_CLOSE_OCCURRENCES.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.COMPOSED_OCCURRENCES.starts(",S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.COMPOSED_OCCURRENCES.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.INC_OCCURRENCES.starts("+S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.INC_OCCURRENCES.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.OPT_OCCURRENCES.starts("?S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.OPT_OCCURRENCES.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.OPT_REQ_OCCURRENCES.starts("*S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.MAND_OCCURRENCES.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.MAND_OCCURRENCES.starts("+S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.OPEN_OCCURRENCES.starts("AxS1_abcbdd1"), equalTo(false));
		assertThat(XRegex.OPEN_OCCURRENCES.starts("<S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.ANY_CHAR_NOTPERCENT_NOTGT_TAIL.starts("S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.ANY_CHAR_NOTPERCENT_NOTGT_TAIL.starts("%S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.ANY_CHAR_NOTPERCENT_NOTGT_TAIL.starts(">S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.ANY_CHAR_NOTQUOTE_NOTTRACE_TAIL.starts("S1_abcbdd1"), equalTo(true));
		assertThat(XRegex.ANY_CHAR_NOTQUOTE_NOTTRACE_TAIL.starts("'S1_abcbdd1"), equalTo(false));
		assertThat(XRegex.ANY_CHAR_NOTQUOTE_NOTTRACE_TAIL.starts("-S1_abcbdd1"), equalTo(false));
	}
	
	
}