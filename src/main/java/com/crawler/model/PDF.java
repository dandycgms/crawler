package com.crawler.model;

import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.util.PDFTextStripperByArea;

public class PDF extends PDFTextStripperByArea {

	private static final Integer MARGINS[] = {30,50,30,40};
	
	private COSDocument cosDoc;
	private PDDocument pdfDoc;
	private List<PDPage> pdfPages;
	private List<String> strPages;
	private File file;
	

    public PDF(File file) throws IOException {
        super.setSortByPosition(false); // para leituras na horizontal
        this.file = file;
        try {
			PDFParser parser = new PDFParser(new FileInputStream(file));
			parser.parse();
			this.cosDoc = parser.getDocument();
			extractPages(new PDDocument(this.cosDoc));
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

    public void putSeparators() {
		List<String> strPagesClone = getStrPages();
		for (int j = 0; j < strPagesClone.size(); j ++)
			getStrPages().set(j, strPagesClone.get(j));
	}
    
	public void clipMargins() {
        PDDocument outputDoc = new PDDocument();
        for (PDPage page : pdfPages) {        	
        	float PAGE_HEIGHT = page.getMediaBox().getHeight();
        	float PAGE_WIDTH = page.getMediaBox().getWidth();
            PDRectangle rectangle=new PDRectangle();
            rectangle.setLowerLeftX(MARGINS[0]);
            rectangle.setLowerLeftY(MARGINS[3]);
            rectangle.setUpperRightX(PAGE_WIDTH - MARGINS[1]);
            rectangle.setUpperRightY(PAGE_HEIGHT-MARGINS[2]);            
            page.setMediaBox(rectangle);
            page.setCropBox(rectangle);
            page.setBleedBox(rectangle);
            page.setArtBox(rectangle);
            outputDoc.addPage(page);
        }
        extractPages(outputDoc);
	}

	public void cleanMargins() {
        PDDocument outputDoc = new PDDocument();
        PDPageContentStream pageCS = null;
        for (PDPage page : pdfPages) {
        	float PAGE_HEIGHT = page.getMediaBox().getHeight();
        	float PAGE_WIDTH = page.getMediaBox().getWidth();
            try {
				pageCS = new PDPageContentStream(pdfDoc, page, true, false);
	            pageCS.setNonStrokingColor(Color.WHITE);
	            pageCS.fillRect(0, 0, PAGE_WIDTH, MARGINS[0]);
	            pageCS.fillRect(PAGE_WIDTH - MARGINS[1], 0, MARGINS[1], PAGE_HEIGHT);
	            pageCS.fillRect(0, PAGE_HEIGHT-MARGINS[2], PAGE_WIDTH, MARGINS[2]);
	            pageCS.fillRect(0, 0, MARGINS[3], PAGE_HEIGHT);
	            pageCS.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
            outputDoc.addPage(page);
        }
        extractPages(outputDoc);
	}

	public void saveAs(String path) {
		if (pdfDoc == null)
			return;
		file = new File(path);
        file.delete();
        try {
			pdfDoc.save(file);
		} catch (COSVisitorException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void saveAsCrop() {
		saveAs(file.getPath().replace("\\resources\\pdfs", "\\resources\\crops"));
	}
	
	public List<String> stripPage(int i, int j) {
		return getStrPages().subList(i-1, j);
	}

	public List<String> getStrPages() {
		if ( strPages != null )
			return strPages;
		strPages = new ArrayList<String>();
		try {
			for (PDPage page : pdfPages) {
				Rectangle2D.Double d = new Rectangle2D.Double(0, 0, page.getMediaBox().getWidth(), page.getMediaBox().getHeight());
				addRegion("textArea", d);
				extractRegions(page);
				strPages.add(getTextForRegion("textArea"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return strPages;
	}

	@SuppressWarnings("unchecked")
	private void extractPages(PDDocument pdDocument) {
		this.pdfDoc = pdDocument;
		this.pdfPages = pdfDoc.getDocumentCatalog().getAllPages();
		this.strPages = null;
		putSeparators();
	}
	
	public void close() {
		try {
			if (cosDoc != null)
				this.cosDoc.close();
			if (pdfDoc != null)
				this.pdfDoc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}