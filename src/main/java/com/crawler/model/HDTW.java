package com.crawler.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.crawler.enums.TokenOption;
import com.crawler.tokenizer.Token;
import com.crawler.tokenizer.Tokenizer;
import com.crawler.util.ArrayUtil;
import com.crawler.util.StringUtil;

public class HDTW {
	
	private static final double LOG2 = Math.log(2);

	private TokenOption opt;
	private Cell BLANK_CELL;
	private Matrix matrix;
	private Word result;
	private double score;
	
	public HDTW(TokenOption opt) {
		this.opt = opt;
		BLANK_CELL = new Cell(new Token(StringUtil.blank, opt));
	}
	
	public Word getResult() {
		return result;
	}

	public void setResult(Word result) {
		this.result = result;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "Score: " + getScore() + "\nResult:" + getResult();
	}
	
	public void execute(String ... s) {
		if (s.length < 2)
			throw new RuntimeException("São esperados pelo menos duas strings");
		Integer[] indexes = ArrayUtil.getDescendingSortedIndexes(StringUtil.getStringCount(s));
		System.out.println("Align #1 & #2");
		matrix = new Matrix(Tokenizer.splitInTokens(s[indexes[0]], opt), Tokenizer.splitInTokens(s[indexes[1]], opt));
		matrix.execute();
		for (int i = 2; i < s.length; i++) {
			System.out.println("Realign #" + (i + 1));
			String string = s[indexes[i]];
			matrix = new Matrix(Tokenizer.splitInTokens(string, opt), matrix.getResult());
			matrix.execute();
		}
		setResult(matrix.getResult());
		setScore(matrix.getScore());
	}
	
	private class Cell {

		private List<Token> vector;

		private Cell() {
			vector = new ArrayList<Token>();
		}

		public Cell(Token t) {
			this();
			add(t);
		}
		
		public void add(Token t) {
			vector.add(t);
		}
		
		public void add(Cell c) {
			vector.addAll(c.vector);
		}
		
		public double cost(Cell x) {
			Map<Token, Double> entropy = new HashMap<Token, Double>();
			int n = this.size() + x.size(); 
			calcHisto(entropy, this, n);
			calcHisto(entropy, x, n);
			double h = 0;
			for (Double p : entropy.values())
				h += p > 0.0 ? - p * Math.log(p)/LOG2 : 0.0;
			return h * (weight() + x.weight());
		}

		private double weight() {
			double x = 0;
			for (Token token : vector)
				x += token.weigth();
			return x;
		}
		
		private int size() {
			return vector.size();
		}

		private void calcHisto(Map<Token, Double> entropy, Cell c, int n) {
			for (Token token : c.vector) {
				if (!entropy.containsKey(token))
					entropy.put(token, 0.0);
				entropy.put(token, entropy.get(token) + 1.0/n);
			}
		}
		
		@Override
		public String toString() {
			return vector.toString();
		}
	}

	/* 
	 * AUXILIAR CLASSES
	 */
	private class Word {
		
		private List<Cell> word;
		
		private Word() {
			word = new ArrayList<Cell>();
		}
		
		public Word(List<Token> tokens) {
			this();
			for (Token token : tokens)
				word.add(new Cell(token));
		}
		
		public int size() {
			return word.size();
		}
		
		public Cell get(int i) {
			return word.get(i);
		}
		
		public void add(Cell c) {
			word.add(c);
		}
		
		private void add(Cell cel1, Cell cel2) {
			Cell c = new Cell();
			c.add(cel1);
			c.add(cel2);
			add(c);
		}
		
		@Override
		public String toString() {
			String ret = "";
			int i = 0;
			boolean hasToken = true;
			while(hasToken) {
				hasToken = false;
				String cand = "\n\t";
				for (Cell cell : word) {
					if (cell.vector.size() > i) {
						hasToken = true;
						cand += cell.vector.get(i); 
					} else
						cand += BLANK_CELL.vector.get(0);
				}
				i++;
				if (hasToken)
					ret += cand;
			}
			return ret;
		}

		public Word reverse() {
			Collections.reverse(this.word);
			return this;
		}
	}
	
	/* 
	 * X = [<a,a,a,b>, <b,b,b,b>, <b,c,c>, <c,c,c,c,c>]
	 * A = [a, b, c, b, a, c, c]
	 * B = [a, b, b, c, c]
	 * 
	 * P(A[0,n-1], B[0,m-1]) = max{
	 * 								c(A[0], B[0]) + P(A[1,n-1], B[1,m-1]) 
	 * 								c(A[0], '-')  + P(A[1,n-1], B[0,m-1]) 
	 * 								c('-', B[0])  + P(A[0,n-1], B[1,m-1]) 
	 * 							  } 
	 */

	private class Matrix {
		
		private Word word1, word2;
		private int n1, n2;
		private Pointer mPoint[][];
		private double mScore[][];
		
		public Matrix(Word word1, Word word2) {
			this.word1 = word1;
			this.word2 = word2;
			this.n1 = word1.size()+1;
			this.n2 = word2.size()+1;
			this.mPoint = new Pointer[n1][n2];
			this.mScore = new double[n1][n2];
		}
		
		public Matrix(List<Token> tokens1, List<Token> tokens2) {
			this(new Word(tokens1), new Word(tokens2));
		}
		
		public Matrix(List<Token> tokens1, Word word2) {
			this(new Word(tokens1), word2);
		}
		
		public Word getResult() {
			Word ret = new Word();
			int i = n1-1;
			int j = n2-1;
			while ( !(i == 0 && j == 0) ) {
				if (mPoint[i][j].equals(Pointer.UP)) {
					ret.add(word1.get(i-1), BLANK_CELL);
					i--;
				} else if (mPoint[i][j].equals(Pointer.LEFT)) {
					ret.add(BLANK_CELL, word2.get(j-1));
					j--;
				} else {
					ret.add(word1.get(i-1),word2.get(j-1));
					i--;
					j--;
				}
			}
			return ret.reverse();
		}

		public double getScore() {
			return mScore[n1-1][n2-1];
		}
		
		public void execute() {
			init();
			for (int i = 1; i < n1; i++) {
				for (int j = 1; j < n2; j++) {
					double u = word1.get(i-1).cost(BLANK_CELL) + mScore[i-1][j];
					double l = word2.get(j-1).cost(BLANK_CELL) + mScore[i][j-1];
					double d = word1.get(i-1).cost(word2.get(j-1)) + mScore[i-1][j-1];
					if (u < l && u < d) {
						mPoint[i][j] = Pointer.UP;
						mScore[i][j] = u;
					} else if (l < d) {
						mPoint[i][j] = Pointer.LEFT;
						mScore[i][j] = l;
					} else {
						mPoint[i][j] = Pointer.DIAG;
						mScore[i][j] = d;
					}
				}
			}
		}

		private void init() {
			mPoint[0][0] = Pointer.STOP;
			mScore[0][0] = 0.0;
			for (int i = 1; i < n1; i++) {
				mPoint[i][0] = Pointer.UP;
				mScore[i][0] = mScore[i-1][0] + word1.get(i-1).cost(BLANK_CELL);
			}
			for (int j = 1; j < n2; j++) {
				mPoint[0][j] = Pointer.LEFT;
				mScore[0][j] = mScore[0][j-1] + word2.get(j-1).cost(BLANK_CELL);
			}
		}
		
		@Override
		public String toString() {
			String ret = "Pointers:";
			for (Pointer[] pointers : mPoint) {
				ret += "\n\t";
				for (Pointer pointer : pointers)
					ret += pointer + " ";
			}
			ret = "Scores:";
			for (double[] scores : mScore) {
				ret += "\n\t";
				for (double score : scores)
					ret += String.format("%02d", (int)score) + " ";
			}
			return ret;
		}
	}

	private enum Pointer {
		LEFT, DIAG, UP, STOP;
	}
	
}
