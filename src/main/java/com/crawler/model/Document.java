package com.crawler.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.crawler.enums.TokenOption;
import com.crawler.tokenizer.Token;
import com.crawler.tokenizer.Tokenizer;
import com.crawler.util.StringUtil;

public class Document {

	private String url;
	private File file;
	private PDF pdf;
	
	public Document(String url) {
		this.url = url;
		this.file = new File(this.url);
		try {
			this.pdf = new PDF(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Apenas oculta textos sobre a margem, mas ainda as tem como texto interno **/
	public Document cleanMargins() {
		pdf.cleanMargins();
		return this;
	}
	
	public Document clipMargins() {
		pdf.clipMargins();
		return this;
	}

	public Document saveAs(String path) {
		pdf.saveAs(path);
		return this;
	}
	
	public Document saveAsCrop() {
		pdf.saveAsCrop();
		return this;
	}
	
	public List<Token> getTokensFromFirstPage(TokenOption opt) {
		return getTokensFrom(1, opt);
	}
	
	public String getFirstCleanPage() {
		return getCleanPage(1);
	}

	public String getFirstPage() {
		return getPage(1);
	}

	public List<Token> getTokensFrom(int p, TokenOption opt) {
		return Tokenizer.splitInTokens(getCleanPage(p), opt);
	}
	
	public String getCleanPage(int p) {
		return StringUtil.cleanEscapes(getPage(p));
	}
	
	public String getPage(int p) {
		List<String> stripPage = stripPage(p, p);
		return stripPage.size() > 0 ? stripPage.get(0) : "";
	}
	
	public List<String> stripPage(int i, int j) {
		return this.pdf.stripPage(i, j);
	}
	
	public List<String> getPages() {
		return this.pdf.getStrPages();
	}

	public List<String> getCleanPages() {
		List<String> ret = new ArrayList<String>();
		for (String string : this.pdf.getStrPages())
			ret.add(StringUtil.cleanEscapes(string));
		return ret;
	}

	public void close() {
		this.pdf.close();
	}

	public PDF getPDF() {
		return pdf;
	}
}
