package com.crawler.tokenizer;

import com.crawler.enums.KindToken;
import com.crawler.enums.TokenOption;

public class Token {

	public String value;
	public String ID;
	public KindToken kind;
	public TokenOption opt;

	public Token(String value, TokenOption opt) {
		this.opt = opt;
		this.value = value;
		if (opt.equals(TokenOption.NONE)){
			this.kind = KindToken.ANY;
			this.ID = value;
		} else {
			this.kind = KindToken.whats(value);
			this.ID = value.toLowerCase();
		}
	}
	
	public double weigth() {
		return kind.weight();
	}

	public boolean startsWith(String a) {
		return toString().startsWith(a);
	}

	public char first() {
		return toString().charAt(0);
	}

	public boolean matches(String string) {
		return toString().matches(string);
	}
		
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		if (TokenOption.ID.equals(opt))
			return prime * result + ((ID == null) ? 0 : ID.hashCode());
		if (TokenOption.KIND.equals(opt))
			return prime * result + ((kind == null) ? 0 : kind.hashCode());
		if (TokenOption.VALUE.equals(opt))
			return prime * result + ((value == null) ? 0 : value.hashCode());
		return prime;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Token other = (Token) obj;
		if (TokenOption.NONE.equals(opt) || TokenOption.VALUE.equals(opt)) {
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
		} else if (TokenOption.ID.equals(opt)) {
			if (ID == null) {
				if (other.ID != null)
					return false;
			} else if (!ID.equals(other.ID))
				return false;
		} else if (TokenOption.KIND.equals(opt)) {
			if (kind == null) {
				if (other.kind != null)
					return false;
			} else if (!kind.equals(other.kind))
				return false;			
		} 
		return true;
	}

	@Override
	public String toString() {
		if (TokenOption.NONE.equals(opt) || TokenOption.VALUE.equals(opt))
			return value;
		if (TokenOption.ID.equals(opt))
			return ID;
		if (TokenOption.KIND.equals(opt))
			return kind.name;
		return "";
	}

}
