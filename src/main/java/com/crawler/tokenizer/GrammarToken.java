
package com.crawler.tokenizer;

public enum GrammarToken {
	Substantivo,
	//É a palavra que dá nome aos objetos, aos lugares, às ações, aos seres em geral, entre outros e varia em gênero (masculino e feminino), número (singular e plural) e grau (aumentativo e diminutivo).
	//Quanto à formação, o substantivo pode ser:
	//Primitivo – é o nome que não deriva de outra palavra da língua portuguesa. Exemplos: casa, pedra e jornal.
	//Derivado – é o nome que deriva de outra palavra da língua portuguesa. Exemplos: casarão, pedreira e jornaleiro (palavras derivadas dos exemplos acima, respetivamente).
	//Simples – é o nome formado por apenas um radical. Radical é o elemento que é a base do significado das palavras. Exemplos: casa, flor e gira, cujos radicais são respetivamente: cas, flor e gir.
	//Composto – é o nome formado por mais do que um radical. Exemplos: couve-flor, girassol e passatempo, cujos radicais são respetivamente: couv e flor, gir e sol e pass e temp.
	//Quanto ao elemento que nomeia, o substantivo pode ser:
	//Comum – é a palavra que dá nome aos elementos da mesma espécie, de forma genérica. Exemplos: cidade, pessoa e rio.
	//Próprio – é a palavra que dá nome aos elementos de forma específica, por isso, são sempre grafados com letra maiúscula. Exemplos: Bahia, Ana e Tietê.
	//Concreto – é a palavra que dá nome aos elementos concretos, de existência real ou imaginária. Exemplos: casa, fada e pessoa.
	//Coletivo – é a palavra que dá nome ao grupo de elementos da mesma espécie. Exemplos: acervo (conjunto de obras de arte), cardume (conjunto de peixes) e resma (conjunto de papéis).
	//Abstrato – é a palavra que dá nome a ações, estados, qualidades e sentimentos. Exemplos: trabalho, alegria, altura e amor.
	Artigo,
	//É a palavra que antecede os substantivos e varia em gênero e número, bem como o determina (artigo definido) ou o generaliza (artigo indefinido).
	//São artigos definidos: o, a (no singular) e os, as (no plural)
	//São artigos indefinidos: um, uma (no singular) e uns, umas (no plural)
	Adjetivo,
	//É a palavra que caracteriza, atribui qualidades aos substantivos e varia em gênero, número e grau.
	//Quanto à formação, o adjetivo pode ser:
	//Primitivo – é o adjetivo que dá origem a outros adjetivos. Exemplos: alegre, bom e fiel.
	//Derivado – é o adjetivo que deriva de substantivos ou verbos. Exemplos: alegria e bondade (palavras derivadas dos exemplos acima, respetivamente) e escritor (palavra derivada do verbo escrever).
	//Simples – é o adjetivo formado por apenas um radical. Exemplos: alta, estudioso e honesto, cujos radicais são respetivamente: alt, estud e honest.
	//Composto – é o adjetivo formado por mais do que um radical. Exemplos: superinteressante, surdo-mudo e verde-claro, cujos radicais são respetivamente: super e interessant, surd e mud e verd e clar.
	//Há também os Adjetivos Pátrios, que caracterizam os substantivos de acordo com o seu local de origem e as Locuções Adjetivas, que são o conjunto de palavras que tem valor de adjetivo.
	//Exemplos de Adjetivos Pátrios: brasileiro, carioca e sergipano.
	//Exemplos de Locuções Adjetivas: de anjo (=angelical), de mãe (=maternal) e de face (=facial).
	Numeral,
	//É a palavra que indica a posição ou o número de elementos.
	//Os numerais classificam-se em:
	//Cardinais – é a forma básica dos números, utilizada na sua contagem. Exemplos: um, dois e vinte.
	//Ordinais – é a forma dos números que indica a posição de um elemento numa série. Exemplos: segundo, quarto e trigésimo.
	//Fracionários – é a forma dos números que indica a divisão das proporções. Exemplos: meio, metade e um terço.
	//Coletivos – é a forma dos números que indica um conjunto de elementos. Exemplos: uma dúzia (conjunto de doze), semestre (conjunto de seis) e centena (conjunto de cem).
	//Multiplicativos – é a forma dos números que indica multiplicação. Exemplos: dobro, duplo e sêxtuplo.
	Pronome,
	//É a palavra que substitui ou acompanha o substantivo, indicando a relação das pessoas do discurso e varia em gênero, número e pessoa.
	//Os pronomes classificam-se em:
	//Pessoais – Caso reto (quando são o sujeito da oração): eu, tu, ele/ela, nós, vós, eles/elas e Caso oblíquo (quando são complemento da oração): me, mim, comigo, te, ti, contigo, o, a, lhe, se, si, consigo, nos, conosco, vos, convosco, os, as lhes, se, si, consigo.
	//Tratamento – Alguns exemplos: Você, Senhor e Vossa Excelência.
	//Possessivos – meu, teu, seu, nosso, vosso, seu e respetivas flexões.
	//Demonstrativos – este, esse, aquele e respetivas flexões, isto, isso, aquilo.
	//Relativos – o qual, a qual, cujo, cuja, quanto e respetivas flexões, quem, que, onde.
	//Indefinidos – algum, alguma, nenhum, nenhuma, muito, muita, pouco, pouca, todo, toda, outro, outra, certo, certa, vário, vária, tanto, tanta, quanto, quanta, qualquer, qual, um, uma e respetivas flexões e quem, alguém, ninguém, tudo, nada, outrem, algo, cada.
	//Interrogativos – qual, quais, quanto, quanta, quantas, quem, que.
	Verbo,
	//É a palavra que exprime ação, estado, mudança de estado, fenômeno da natureza e varia em pessoa (primeira, segunda e terceira), número (singular e plural), tempo (presente, passado e futuro), modo (indicativo, subjuntivo e imperativo) e voz (ativa, passiva e reflexiva).
	//Exemplos:
	//O time adversário marcou gol. (ação)
	//Estou tão feliz hoje! (estado)
	//De repente ficou triste (mudança de estado)
	//Trovejava sem parar. (fenômeno da natureza)
	Adverbio,
	//É a palavra que modifica o verbo, o adjetivo ou outro advérbio, exprimindo circunstâncias de tempo, modo, intensidade, entre outros.
	//Os advérbios classificam-se em:
	//Modo – Exemplos: assim, devagar e grande parte das palavras terminadas em “-mente”.
	//Intensidade – Exemplos: demais, menos e tão.
	//Lugar – Exemplos: adiante, lá e fora.
	//Tempo – Exemplos: ainda, já e sempre.
	//Negação – Exemplos: não, jamais e tampouco.
	//Afirmação – Exemplos: certamente, certo e sim.
	//Dúvida – acaso, quiçá e talvez.
	Preposicao,
	//É a palavra que liga dois elementos da oração.
	//As preposições classificam-se em:
	//Essenciais – têm somente função de preposição. Exemplos: a, desde e para.
	//Acidentais – não têm propriamente a função de preposição, mas podem funcionar como tal. Exemplos: como, durante e exceto.
	//Há também as Locuções Prepositivas, que são o conjunto de palavras que tem valor de preposição. Exemplos: apesar de, em vez de e junto de.
	Conjuncao,
	//É a palavra que liga duas orações.
	//As conjunções classificam-se em:
	//Coordenativas: Aditivas (e, nem), Adversativas (contudo, mas), Alternativas (ou…ou, seja…seja), Conclusivas (logo, portanto) e Explicativas (assim, porquanto).
	//Subordinativas: Integrantes (que, se), Causais (porque, como), Comparativas (que, como), Concessivas (embora, posto que), Condicionais (caso, salvo se), Conformativas (como, segundo), Consecutivas (que, de maneira que), Temporais (antes que, logo que), Finais (a fim de que, para que) e Proporcionais (ao passo que, quanto mais).
	//Há também as Locuções Conjuntivas, que são o conjunto de palavras que tem valor de conjunção. Exemplos: contanto que, logo que e visto que.
	
	Interjeicao;
	//É a palavra que exprime emoções e sentimentos.
	//As interjeições podem ser classificadas em:
	//Advertência – Calma!, Devagar!, Sentido!
	//Saudação – Alô!, Oi!, Tchau!
	//Ajuda – Ei!, Ô!, Socorro!
	//Afugentamento – Fora!, Sai! Xô!
	//Alegria – Eba!, Uhu! Viva!
	//Tristeza – Oh!, Que pena!, Ui!
	//Medo – Credo!, Cruzes!, Jesus!
	//Alívio – Arre!, Uf!, Ufa!
	//Animação – Coragem!, Força!, Vamos!
	//Aprovação – Bis!, Bravo!, Isso!
	//Desaprovação – Chega!, Francamente! Livra!,
	//Concordância –Certo!, Claro!, Ótimo!
	//Desejo – Oxalá!, Quisera!, Tomara!
	//Desculpa – Desculpa!, Opa!, Perdão!
	//Dúvida – Hã?, Hum?, Ué!
	//Espanto – Caramba!, Oh!, Xi!,
	//Contrariedade – Credo!, Droga!, Porcaria!
	//Há também as Locuções Interjetivas, que são o conjunto de palavras que tem valor de conjunção. Exemplos: Cai fora!, Muito obrigada!, Volta aqui!
}
