package com.crawler.tokenizer;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.crawler.enums.TokenOption;
import com.crawler.util.StringUtil;

public class Tokenizer {

	public static List<Token> splitInTokens(String str) {
		return splitInTokens(str, TokenOption.NONE, "");
	}
	
	public static List<Token> splitInTokens(String str, TokenOption tko) {
		return splitInTokens(str, tko, null);
	}
	
	public static List<Token> splitInTokens(String str, TokenOption tko, String separator) {
		List<Token> r = new ArrayList<Token>();
		String strsep = "";
		if (separator == null)
			strsep = applySeparators(str, StringUtil.separators_chars);
		else if (separator.isEmpty()) {
			for (byte c : str.getBytes()) 
				r.add(new Token(String.valueOf((char)c), tko));
			return r;
		} else
			strsep = applySeparators(str, separator);
		for (String v : StringUtil.cleanEscapes(strsep).split(" "))
			if (!v.isEmpty())
				r.add(new Token(v, tko));
		return r;
	}

	public static String applySeparators(String str, String separators_chars) {
		Pattern pattern = Pattern.compile("([" + separators_chars + "])");
		Matcher matcher = pattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find())
			matcher.appendReplacement(sb, " $1 ");
		matcher.appendTail(sb);
		return sb.toString();
	}
	
}
