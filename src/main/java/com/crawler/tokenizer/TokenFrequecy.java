package com.crawler.tokenizer;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.crawler.util.ArrayUtil;

public class TokenFrequecy {
	public static Map<String, List<Token>> frequecies;
	
	public static Map<String, List<Token>> getFrequecies() {
		if (frequecies == null)
			frequecies = new LinkedHashMap<String, List<Token>>();
		return frequecies;
	}

	public static void show() {
		List<String> keys = new ArrayList<String>(frequecies.keySet());
		List<List<Token>> freq = new ArrayList<List<Token>>(frequecies.values());
		List<Integer> freqcount = getFreqCount(freq);
		Integer[] indexes = ArrayUtil.getAscendingSortedIndexes(freqcount); 
		for (Integer i : indexes)
			System.out.println(" > " + freqcount.get(i) + ": \"" + keys.get(i) + "\" :: " + freq.get(i));
	}

	private static List<Integer> getFreqCount(List<List<Token>> freq) {
		List<Integer> freqcount = new ArrayList<Integer>();
		for (List<Token> list : freq)
			freqcount.add(list.size());
		return freqcount;
	}
	
	public static void addToken(Token token) {
		String strtoken = token.ID;
		if (!getFrequecies().containsKey(strtoken))
			getFrequecies().put(strtoken,new ArrayList<Token>());
		getFrequecies().get(strtoken).add(token);
	}
	
	public static void addTokens(List<Token> tokens) {
		for (Token token : tokens) 
			addToken(token);
	}
	
	
}