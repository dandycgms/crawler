package com.crawler.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FilesUtil {

	public static List<String> listFilesForFolderRec(String path) {
		final File folder = new File(path);
		List<String> files = new ArrayList<String>();
		for (final File fileEntry : folder.listFiles()) {
			try {
				if (fileEntry.isDirectory())
					files.addAll(listFilesForFolderRec(fileEntry.getCanonicalPath()));
				else
					files.add(fileEntry.getCanonicalPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return files;
	}

	public static List<String> listFilesForFolder(String path) {
		final File folder = new File(path);
		List<String> files = new ArrayList<String>();
		for (final File fileEntry : folder.listFiles()) {
			if (!fileEntry.isDirectory())
				try {
					files.add(fileEntry.getCanonicalPath());
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return files;
	}

}
