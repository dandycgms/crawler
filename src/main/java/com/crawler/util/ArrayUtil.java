package com.crawler.util;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class ArrayUtil implements Comparator<Integer> {
    private final List<?> array;

    public ArrayUtil(List<?> array) {
        this.array = array;
    }

    public Integer[] createIndexArray() {
        Integer[] indexes = new Integer[array.size()];
        for (int i = 0; i < array.size(); i++)
            indexes[i] = i; // Autoboxing
        return indexes;
    }

    public static Integer[] getAscendingSortedIndexes(List<?> array) {
	    ArrayUtil comparator = new ArrayUtil(array);
		Integer[] indexes = comparator.createIndexArray();
		Arrays.sort(indexes, comparator);
		return indexes;
    }
    
    public static Integer[] getDescendingSortedIndexes(List<?> array) {
		Integer[] indexesAsc = getAscendingSortedIndexes(array);
		Integer[] indexes = new Integer[indexesAsc.length];
		for (int i = 0; i < indexes.length; i++)
			indexes[i] = indexesAsc[indexesAsc.length-1-i];
		return indexes;
    }
	
	@Override
    @SuppressWarnings("unchecked")
    public int compare(Integer index1, Integer index2) {
         // Autounbox from Integer to int to use as array indexes
        return ((Comparable<Object>)array.get(index1)).compareTo(array.get(index2));
    }
	
	public static String[] concatenate (String[] a, String[] b) {
	    return Stream.concat(Arrays.stream(a), Arrays.stream(b)).toArray(String[]::new);
	}
}