package com.crawler.enums;

import com.crawler.util.StringUtil;

public enum KindToken {
	
	BLANK("_",1),
	BREAK("B",100),
	PONCTUATION("P",5), 
	DELIMITER("D",100), 
	NUMBER("N",1), 
	UPPER("U",1), 
	FIRSTUPPER("F",1), 
	LOWER("L",1), 
	ALPHANUM("@",1),
	ALPHA("A",1),
	ANY("*",1);
	
	public String name;
	public double weight;
	
	KindToken(String name, double weight) {
		this.name = name;
		this.weight = weight;
	}
	
	public double weight() {		
		return weight;
	}
	
	public static boolean isBlank(String value) {
		return value.equals(StringUtil.blank);
	}
	
	public static boolean isBreak(String value) {
		return value.equals(StringUtil.breaker);
	}
	
	public static boolean isDelimiter(String value) {
		return value.matches("[" + StringUtil.delimiters + "]");
	}

	public static boolean isPonctuation(String value) {
		return value.matches("[" + StringUtil.ponctuation + "]");
	}

	public static boolean isAllDigit(String value) {
		return value.matches("[" + StringUtil.digits + "]+");
	}

	public static boolean isAllAlpha(String value) {
		return value.matches("[" + StringUtil.alpha + StringUtil.ALPHA + "]+");
	}

	public static boolean isAllLowerCase(String value) {
		return value.matches("[" + StringUtil.alpha + "]+");
	}

	public static boolean isAllUpperCase(String value) {
		return value.matches("[" + StringUtil.ALPHA + "]+");
	}

	public static boolean isFirstUpperCase(String value) {
		return value.matches("[^" + StringUtil.ALPHA + "]");
	}

	public static KindToken whats(String value) {
		if (KindToken.isBlank(value))			return KindToken.BLANK;
		if (KindToken.isBreak(value)) 			return KindToken.BREAK;
		if (KindToken.isPonctuation(value)) 	return KindToken.PONCTUATION;
		if (KindToken.isDelimiter(value)) 		return KindToken.DELIMITER;
		if (KindToken.isAllDigit(value)) 		return KindToken.NUMBER;
		if (KindToken.isFirstUpperCase(value)) 	return KindToken.FIRSTUPPER;
		if (KindToken.isAllUpperCase(value)) 	return KindToken.UPPER;
		if (KindToken.isAllLowerCase(value)) 	return KindToken.LOWER;
		if (KindToken.isAllAlpha(value)) 		return KindToken.ALPHA;
		return KindToken.ALPHANUM;

	}
}
