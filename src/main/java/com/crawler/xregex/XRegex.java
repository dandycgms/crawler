package com.crawler.xregex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.crawler.tokenizer.Token;
import com.crawler.util.StringUtil;

public enum XRegex implements AbstractXRegex {

	// ///////////////////////////////////////////////////////////////////////////////////////////////////
	// STRINGS
	// ///////////////////////////////////////////////////////////////////////////////////////////////////

	ALPHA_STARTED_WITH_LETTER {
		/*
		 * ALPHA_STARTED_WITH_LETTER -> '\a' ALPHA_STARTED_WITH_LETTER_TAIL
		 */
		protected String extract() {
			return firstConsumes(tokens) + XRegex.ALPHA_STARTED_WITH_LETTER_TAIL.load(tokens).extract();
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'));
		}
	},
	ALPHA_STARTED_WITH_LETTER_TAIL {
		/*
		 * ALPHA_STARTED_WITH_LETTER_TAIL -> [\@_] ALPHA_STARTED_WITH_LETTER_TAIL
		 */
		protected String extract() {
			if (starts(tokens))
				return advance(tokens) + ALPHA_STARTED_WITH_LETTER_TAIL.load(tokens).extract();
			return "";
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('_','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9'));
		}
	},
	NUMBER {
		/*
		 * NUMBER -> '\d' NUMBER_TAIL
		 */
		protected String extract() {
			return firstConsumes(tokens) + NUMBER_TAIL.load(tokens).extract();
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('0','1','2','3','4','5','6','7','8','9'));
		}
	},
	NUMBER_TAIL {
		/*
		 * NUMBER_TAIL -> NUMBER
		 */
		protected String extract() {
			if (starts(tokens))
				return advance(tokens) + NUMBER_TAIL.load(tokens).extract();
			return "";
		}

		public void setFirsts() {
			firsts = NUMBER.firsts();
		}
	},
	COLISION {
		/*
		 * *COLISION -> 'x' NUMBER
		 */
		protected String extract() {
			if ( starts(tokens)) {
				advance(tokens);
				return (String) NUMBER.load(tokens).extract();
			}
			return null;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('x'));
		}
	},
	COMPOSED_OCCURRENCES {
		/*
		 * COMPOSED_OCCURRENCES -> ',' NUMBER SINGLE_CLOSE_OCCURRENCES
		 */
		protected String extract() {
			consumes(tokens, ",");
			String ret = (String) NUMBER.load(tokens).extract();
			SINGLE_CLOSE_OCCURRENCES.load(tokens).extract();
			return ret;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList(','));
		}
	},
	ANY_CHAR_NOTQUOTE {
		/*
		 * ANY_CHAR_NOTQUOTE -> ~'\'' ANY_CHAR_NOTQUOTE
		 */
		protected String extract() {
			if (starts(tokens))
				return advance(tokens) + ANY_CHAR_NOTQUOTE.load(tokens).extract();
			return "";
		}
		
		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('\''));
		}
		
		public boolean signsearch() {
			return false;
		}
	},
	ANY_CHAR_NOTQUOTE_NOTTRACE  {
		/*
		 * ANY_CHAR_NOTQUOTE_NOTTRACE 	-> ~'\'|-' ANY_CHAR_NOTQUOTE_NOTTRACE_TAIL
		 */
		protected String extract() {
			return firstConsumes(tokens) + ANY_CHAR_NOTQUOTE_NOTTRACE_TAIL.load(tokens).extract();
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('\'', '-'));
		}
		
		public boolean signsearch() {
			return false;
		}
	},
	ANY_CHAR_NOTQUOTE_NOTTRACE_TAIL  {
		/*
		 * ANY_CHAR_NOTQUOTE_NOTTRACE_TAIL 	-> ~'\'|-' ANY_CHAR_NOTQUOTE_NOTTRACE_TAIL
		 */
		protected String extract() {
			if (starts(tokens))
				return advance(tokens) + ANY_CHAR_NOTQUOTE_NOTTRACE_TAIL.load(tokens).extract();
			return "";
		}

		public void setFirsts() {
			firsts = ANY_CHAR_NOTQUOTE_NOTTRACE.firsts();
		}
		
		public boolean signsearch() {
			return false;
		}
	},
	QUOTED_REGEX {
		/*
		 * QUOTED_REGEX -> '\'ANY_CHAR_NOTQUOTE\''
		 */
		protected String extract() {
			consumes(tokens, "'");
			String value = (String) ANY_CHAR_NOTQUOTE.load(tokens).extract();
			consumes(tokens, "'");
			return value;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('\''));
		}
	},
	INTERVAL_TAIL {
		/*
		 * *INTERVAL_TAIL		-> '-' ANY_CHAR_NOTQUOTE_NOTTRACE
		 */
		protected Object extract() {
			if (starts(tokens)) {
				advance(tokens);
				return ANY_CHAR_NOTQUOTE_NOTTRACE.load(tokens).extract();
			}
			return null;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('-'));
		}
	},
	ANY_CHAR_NOTPERCENT {
		/*
		 * ANY_CHAR_NOTPERCENT -> ~'%' ANY_CHAR_NOTPERCENT_TAIL
		 */
		protected String extract() {
			return firstConsumes(tokens) + ANY_CHAR_NOTPERCENT_TAIL.load(tokens).extract();
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('%'));
		}
		
		public boolean signsearch() {
			return false;
		}
	},
	ANY_CHAR_NOTPERCENT_TAIL {
		/*
		 * ANY_CHAR_NOTPERCENT_TAIL -> ~'%' ANY_CHAR_NOTPERCENT_TAIL
		 */
		protected String extract() {
			if (starts(tokens))
				return advance(tokens) + ANY_CHAR_NOTPERCENT_TAIL.load(tokens).extract();
			return "";
		}

		public void setFirsts() {
			firsts = ANY_CHAR_NOTPERCENT.firsts();
		}
		
		public boolean signsearch() {
			return false;
		}
	},
	ANY_CHAR_NOTPERCENT_NOTGT {
		/*
		 * ANY_CHAR_NOTPERCENT_NOTGT -> ~'%>' ANY_CHAR_NOTPERCENT_NOTGT_TAIL
		 */
		protected String extract() {
			return firstConsumes(tokens) + ANY_CHAR_NOTPERCENT_NOTGT_TAIL.load(tokens).extract();
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('%','>'));
		}
		
		public boolean signsearch() {
			return false;
		}
	},
	ANY_CHAR_NOTPERCENT_NOTGT_TAIL {
		/*
		 * ANY_CHAR_NOTPERCENT_NOTGT_TAIL -> ~'%>' ANY_CHAR_NOTPERCENT_NOTGT_TAIL
		 */
		protected String extract() {
			if (starts(tokens))
				return advance(tokens) + ANY_CHAR_NOTPERCENT_NOTGT_TAIL.load(tokens).extract();
			return "";
		}

		public void setFirsts() {
			firsts = ANY_CHAR_NOTPERCENT_NOTGT.firsts();
		}
		
		public boolean signsearch() {
			return false;
		}
	},
	MAND_SCHAR {
		/*
		 * MAND_SCHAR -> ANY_CHAR_NOTPERCENT
		 */
		protected String extract() {
			return (String) ANY_CHAR_NOTPERCENT.load(tokens).extract();
		}

		public void setFirsts() {
			firsts = ANY_CHAR_NOTPERCENT.firsts();
		}
		
		public boolean signsearch() {
			return false;
		}
	},
	OPT_SCHAR {
		/*
		 * OPT_SCHAR -> ANY_CHAR_NOTPERCENT
		 */
		protected String extract() {
			if (starts(tokens))
				return (String) ANY_CHAR_NOTPERCENT.load(tokens).extract();
			return "";
		}

		public void setFirsts() {
			firsts = ANY_CHAR_NOTPERCENT.firsts();
		}
		
		public boolean signsearch() {
			return false;
		}
	},
	L_OPT_SCHAR {
		/*
		 * L_OPT_SCHAR -> ANY_CHAR_NOTPERCENT_NOTGT
		 */
		protected String extract() {
			if (starts(tokens))
				return (String) ANY_CHAR_NOTPERCENT_NOTGT.load(tokens).extract();
			return "";
		}

		public void setFirsts() {
			firsts = ANY_CHAR_NOTPERCENT_NOTGT.firsts();
		}
		
		public boolean signsearch() {
			return false;
		}
	},
	INC_OCCURRENCES {
		/*
		 * INC_OCCURENCES -> '+' SINGLE_CLOSE_OCCURRENCES
		 */
		protected String extract() {
			consumes(tokens, "+");
			SINGLE_CLOSE_OCCURRENCES.load(tokens).extract();
			return "+";
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('+'));
		}
	},
	SINGLE_CLOSE_OCCURRENCES {
		/*
		 * SINGLE_CLOSE_OCCURRENCES -> '>'
		 */
		protected String extract() {
			consumes(tokens, ">");
			return ".";
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('>'));
		}
	},
	AND {
		/*
		 * AND -> '&&'
		 */
		protected String extract() {
			consumes(tokens, "&&");
			return "AND";
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('&'));
		}
	},
	OR {
		/*
		 * OR -> '||'
		 */
		protected String extract() {
			consumes(tokens, "||");
			return "OR";
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('|'));
		}
	},

	// ///////////////////////////////////////////////////////////////////////////////////////////////////
	// BOOLEANS
	// ///////////////////////////////////////////////////////////////////////////////////////////////////

	DELIM_SEQ_TAIL {
		/*
		 * *DELIM_SEQ_TAIL -> '!'
		 */
		protected Boolean extract() {
			if (starts(tokens)) {
				advance(tokens);
				return true;
			}
			return false;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('!'));
		}
	},
	VAR_N {
		/*
		 * VAR_N -> '#'
		 */
		protected Boolean extract() {
			firstConsumes(tokens);
			return true;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('#'));
		}
	},
	SPARSE {
		/*
		 * *SPARSE -> '&'
		 */
		protected Boolean extract() {
			if (starts(tokens)) {
				advance(tokens);
				return true;
			}
			return false;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('&'));
		}
	},
	EXCEPT {
		/*
		 * *EXCEPT -> '~'
		 */
		protected Boolean extract() {
			if (starts(tokens)) {
				advance(tokens);
				return false;
			}
			return true;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('~'));
		}
	},
	CASE_INSENSITIVE {
		/*
		 * *CASE_INSENSITIVE -> 'i'
		 */
		protected Boolean extract() {
			if (starts(tokens)) {
				advance(tokens);
				return true;
			}
			return false;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('i'));
		}
	},

	// ///////////////////////////////////////////////////////////////////////////////////////////////////
	// MAPS
	// ///////////////////////////////////////////////////////////////////////////////////////////////////

	SET_SEQ_TAIL {
		/*
		 * SET_SEQ_TAIL -> SEPARATOR SPARSE CASE_INSENSITIVE
		 */
		protected Map<?,?> extract() {
			Map<String, Object> setseqMap = new HashMap<String, Object>();
			if (starts(tokens)) {
				// TODO: Fazer funcionar pra qqer ordem
				setseqMap.put("separator", SEPARATOR.load(tokens).extract());
				setseqMap.put("sparse", SPARSE.load(tokens).extract());
				setseqMap.put("icase", CASE_INSENSITIVE.load(tokens).extract());
				setseqMap.put("colision", COLISION.load(tokens).extract());
			}
			return setseqMap;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>();
			firsts.addAll(SEPARATOR.firsts());
			firsts.addAll(SPARSE.firsts());
			firsts.addAll(CASE_INSENSITIVE.firsts());
			firsts.addAll(COLISION.firsts());
		}
	},
	SEPARATOR {
		/*
		 * SEPARATOR -> 's<' OPT_SCHAR '%' MAND_SCHAR '%' MAND_SCHAR '%' L_OPT_SCHAR '>'
		 */
		protected Map<?,?> extract() {
			Map<String, String> sepMap = new HashMap<String, String>();
			if (starts(tokens)) {
				advance(tokens);
				consumes(tokens, "<");
				sepMap.put("f", (String) OPT_SCHAR.load(tokens).extract());
				consumes(tokens, "%");
				sepMap.put("s", (String) MAND_SCHAR.load(tokens).extract());
				consumes(tokens, "%");
				sepMap.put("p", (String) MAND_SCHAR.load(tokens).extract());
				consumes(tokens, "%");
				sepMap.put("l", (String) L_OPT_SCHAR.load(tokens).extract());
				consumes(tokens, ">");
			}
			return sepMap;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('s'));
		}
	},
	CONDITION_IF {
		/*
		 * CONDITION_IF -> FUNCT_TAIL PROCEDURE_TAIL
		 */
		protected Map<?,?> extract() {
			Map<String, Object> ifMap = new HashMap<String, Object>();
			List<Object> param = new ArrayList<Object>();
			List<?> tail = (List<?>) FUNCT_TAIL.load(tokens).extract();
			if (!tail.isEmpty())
				param.addAll(tail);
			tail = (List<?>) PROCEDURE_TAIL.load(tokens).extract();
			if (!tail.isEmpty())
				param.addAll(tail);
			ifMap.put("if", param); 
			return ifMap;
		}

		public void setFirsts() {
			firsts = FUNCT_TAIL.firsts();
		}
	},
	CONDITIONAL_THEN {
		/*
		 * CONDITIONAL_THEN -> '?' REGEX CONDITIONAL_ELSE
		 */
		protected Map<?,?> extract() {
			Map<String, Object> condMap = new HashMap<String, Object>();
			if (starts(tokens)) {
				advance(tokens);
				condMap.put("then", REGEX.load(tokens).extract());
				Object esle = CONDITIONAL_ELSE.load(tokens).extract();
				if (esle != null)
					condMap.put("else", esle);
			}
			return condMap;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('?'));
		}
	},
	OCCURRENCES_COUNT {
		/*
		 * OCCURRENCES_COUNT -> NUM_OCCURRENCES_COUNT | VAR_OCCURRENCES_COUNT
		 */
		protected Map<?,?> extract() {
			Map<String, Object> countMap = new HashMap<String, Object>();
			if (NUM_OCCURRENCES_COUNT.starts(tokens)) {
				countMap.put("type", "const");
				countMap.put("number", NUM_OCCURRENCES_COUNT.load(tokens).extract());
			} else {
				countMap.put("type", "var");
				countMap.put("number", VAR_OCCURRENCES_COUNT.load(tokens).extract());
			}
			return countMap;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>();
			firsts.addAll(NUM_OCCURRENCES_COUNT.firsts());
			firsts.addAll(VAR_OCCURRENCES_COUNT.firsts());
		}
	},
	OPT_REQ_OCCURRENCES {
		/*
		 * OPT_REQ_OCCURRENCES -> '*'
		 */
		protected Map<?,?> extract() {
			consumes(tokens, "*");
			Map<String, Object> countMap = new HashMap<String, Object>();
			countMap.put("type", "const");
			countMap.put("number", new ArrayList<String>(Arrays.asList("0","+")) );
			return countMap;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('*'));
		}
	},
	MAND_OCCURRENCES {
		/*
		 * MAND_OCCURRENCES -> '+'
		 */
		protected Map<?,?> extract() {
			consumes(tokens, "+");
			Map<String, Object> countMap = new HashMap<String, Object>();
			countMap.put("type", "const");
			countMap.put("number", new ArrayList<String>(Arrays.asList("1","+")) );
			return countMap;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('+'));
		}
	},
	OPT_OCCURRENCES {
		/*
		 * OPT_OCCURRENCES -> '?'
		 */
		protected Object extract() {
			consumes(tokens, "?");
			Map<String, Object> countMap = new HashMap<String, Object>();
			countMap.put("type", "const");
			countMap.put("number", new ArrayList<String>(Arrays.asList("0","1")) );
			return countMap;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('?'));
		}
	},
	FUNCT {
		/*
		 * FUNCT -> ALPHA_STARTED_WITH_LETTER FUNCT_TAIL
		 */
		protected Map<?,?> extract() {
			Map<String, Object> functMap = new HashMap<String, Object>();
			functMap.put("id", ALPHA_STARTED_WITH_LETTER.load(tokens).extract());
			functMap.put("param", FUNCT_TAIL.load(tokens).extract());
			return functMap;
		}

		public void setFirsts() {
			firsts = ALPHA_STARTED_WITH_LETTER.firsts();
		}
	},

	// ///////////////////////////////////////////////////////////////////////////////////////////////////
	// LISTS
	// ///////////////////////////////////////////////////////////////////////////////////////////////////

	VAR_OCCURRENCES_COUNT {
		/*
		 * VAR_OCCURRENCES_COUNT -> VAR_N NUMBER '>'
		 */
		protected List<?> extract() {
			VAR_N.load(tokens).extract();
			String ret = (String) NUMBER.load(tokens).extract();
			consumes(tokens, ">");
			return new ArrayList<String>(Arrays.asList(ret,".")) ;
		}

		public void setFirsts() {
			firsts = VAR_N.firsts();
		}
	},
	NUM_OCCURRENCES_COUNT {
		/*
		 * NUM_OCCURRENCES_COUNT -> NUMBER CLOSE_OCCURRENCES
		 */
		protected List<?> extract() {
			List<Object> ret = new ArrayList<Object>();
			ret.add(NUMBER.load(tokens).extract());
			ret.add(CLOSE_OCCURRENCES.load(tokens).extract());
			return ret;
		}

		public void setFirsts() {
			firsts = NUMBER.firsts();
		}
	},
	DECLARE_LIST {
		/*
		 * DECLARE_LIST -> DECLARE DECLARE_LIST_TAIL
		 */
		protected List<?> extract() {
			List<Object> ret = new ArrayList<Object>();
			ret.add(DECLARE.load(tokens).extract());
			ret.addAll((List<?>) DECLARE_LIST_TAIL.load(tokens).extract());
			return ret;
		}

		public void setFirsts() {
			firsts = DECLARE.firsts();
		}
	},
	DECLARE_LIST_TAIL {
		/*
		 * DECLARE_LIST_TAIL -> ";" DECLARE DECLARE_LIST_TAIL
		 */
		protected List<?> extract() {
			List<Object> ret = new ArrayList<Object>();
			while (starts(tokens)) {
				advance(tokens);
				ret.add(DECLARE.load(tokens).extract());
			}
			return ret;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList(';'));
		}
	},
	REGEX_LIST {
		/*
		 * REGEX_LIST -> REGEX REGEX_LIST_TAIL
		 */
		protected List<?> extract() {
			List<Object> ret = new ArrayList<Object>();
			ret.add(REGEX.load(tokens).extract());
			ret.addAll((List<?>) REGEX_LIST_TAIL.load(tokens).extract());
			return ret;
		}

		public void setFirsts() {
			firsts = REGEX.firsts();
		}
	},
	REGEX_LIST_TAIL {
		/*
		 * REGEX_LIST_TAIL -> ',' REGEX
		 */
		protected List<?> extract() {
			List<Object> ret = new ArrayList<Object>();
			while (starts(tokens)) {
				advance(tokens);
				ret.add(REGEX.load(tokens).extract());
			}
			return ret;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList(','));
		}
	},
	PROCEDURE_TAIL {
		/*
		 * PROCEDURE_TAIL -> LOGICAL_OP FUNCT PROCEDURE_TAIL
		 */
		protected List<?> extract() {
			List<Object> ret = new ArrayList<Object>();
			while (starts(tokens)) {
				ret.add(LOGICAL_OP.load(tokens).extract());
				ret.add(FUNCT.load(tokens).extract());
			}
			return ret;
		}

		public void setFirsts() {
			firsts = LOGICAL_OP.firsts();
		}
	},
	REGEX_LIST_OPT {
		/*
		 * *REGEX_LIST_OPT -> REGEX_LIST
		 */
		protected List<?> extract() {
			List<Object> ret = new ArrayList<Object>();
			if (starts(tokens))
				ret.addAll((List<?>) REGEX_LIST.load(tokens).extract());
			return ret;
		}

		public void setFirsts() {
			firsts = REGEX_LIST.firsts();
		}
	},
	FUNCT_TAIL {
		/*
		 * FUNCT_TAIL -> '(' REGEX_LIST_OPT ')'
		 */
		protected List<?> extract() {
			List<Object> ret = new ArrayList<Object>();
			consumes(tokens, "(");
			ret.addAll((List<?>) REGEX_LIST_OPT.load(tokens).extract());
			consumes(tokens, ")");
			return ret;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('('));
		}
	},

	// ///////////////////////////////////////////////////////////////////////////////////////////////////
	// BYPASSES
	// ///////////////////////////////////////////////////////////////////////////////////////////////////

	LOGICAL_OP {
		/*
		 * LOGICAL_OP -> AND | OR
		 */
		protected Object extract() {
			if (AND.starts(tokens))
				return AND.load(tokens).extract();
			return OR.load(tokens).extract();
		}

		public void setFirsts() {
			firsts = new HashSet<Character>();
			firsts.addAll(AND.firsts());
			firsts.addAll(OR.firsts());
		}
	},
	CONDITIONAL_ELSE {
		/*
		 * CONDITIONAL_ELSE -> ':' REGEX
		 */
		protected Object extract() { // REGEX
			if (starts(tokens)) {
				advance(tokens);
				return REGEX.load(tokens).extract();
			}
			return null;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList(':'));
		}
	},
	CLOSE_OCCURRENCES {
		/*
		 * CLOSE_OCCURRENCES -> SINGLE_CLOSE_OCCURRENCES | COMPOSED_OCCURRENCES | INC_OCCURENCES
		 */
		protected Object extract() { // STR
			if (SINGLE_CLOSE_OCCURRENCES.starts(tokens))
				return SINGLE_CLOSE_OCCURRENCES.load(tokens).extract();
			if (COMPOSED_OCCURRENCES.starts(tokens))
				return COMPOSED_OCCURRENCES.load(tokens).extract();
			return INC_OCCURRENCES.load(tokens).extract();
		}

		public void setFirsts() {
			firsts = new HashSet<Character>();
			firsts.addAll(SINGLE_CLOSE_OCCURRENCES.firsts());
			firsts.addAll(COMPOSED_OCCURRENCES.firsts());
			firsts.addAll(INC_OCCURRENCES.firsts());
		}
	},
	OPEN_OCCURRENCES {
		/*
		 * OPEN_OCCURRENCES -> '<' OCCURRENCES_COUNT
		 */
		protected Object extract() { // MAP
			consumes(tokens, "<");
			return OCCURRENCES_COUNT.load(tokens).extract();
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('<'));
		}
	},
	OCCURRENCES_BODY {
		/*
		 * OCCURRENCES_BODY -> OPEN_OCCURRENCES | OPT_REQ_OCCURRENCES | MAND_OCCURRENCES | OPT_OCCURRENCES
		 */
		protected Object extract() { // MAP
			if (OPEN_OCCURRENCES.starts(tokens))
				return OPEN_OCCURRENCES.load(tokens).extract();
			if (OPT_REQ_OCCURRENCES.starts(tokens))
				return OPT_REQ_OCCURRENCES.load(tokens).extract();
			if (MAND_OCCURRENCES.starts(tokens))
				return MAND_OCCURRENCES.load(tokens).extract();
			return OPT_OCCURRENCES.load(tokens).extract();
		}

		public void setFirsts() {
			firsts = new HashSet<Character>();
			firsts.addAll(OPEN_OCCURRENCES.firsts());
			firsts.addAll(OPT_REQ_OCCURRENCES.firsts());
			firsts.addAll(MAND_OCCURRENCES.firsts());
			firsts.addAll(OPT_OCCURRENCES.firsts());
		}
	},
	LAZY_OCCURRENCES {
		/*
		 * LAZY_OCCURRENCES -> '_' OCCURRENCES_BODY
		 */
		@SuppressWarnings("unchecked")
		protected Object extract() { // MAP
			consumes(tokens, "_");
			Map<String, Object> ret = (Map<String, Object>) OCCURRENCES_BODY.load(tokens).extract();
			ret.put("lazy", true);
			return ret;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('_'));
		}
	},
	OCCURRENCES {
		/*
		 * *OCCURRENCES -> LAZY_OCCURRENCES | OCCURRENCES_BODY
		 */
		protected Object extract() { // MAP
			if (LAZY_OCCURRENCES.starts(tokens))
				return LAZY_OCCURRENCES.load(tokens).extract();
			else if (OCCURRENCES_BODY.starts(tokens))
				return OCCURRENCES_BODY.load(tokens).extract();
			return new HashMap<String, Object>();
		}

		public void setFirsts() {
			firsts = new HashSet<Character>();
			firsts.addAll(LAZY_OCCURRENCES.firsts());
			firsts.addAll(OCCURRENCES_BODY.firsts());
		}
	},
	REGEX_BODY {
		/*
		 * REGEX_BODY -> SET | INTERVAL | SEQUENCE | LOOP | OPERATION | GROUP_REF
		 */
		protected Object extract() { // XRegexNode
			if (INTERVAL.starts(tokens))
				return INTERVAL.load(tokens).extract();
			if (SET.starts(tokens))
				return SET.load(tokens).extract();
			if (SEQUENCE.starts(tokens))
				return SEQUENCE.load(tokens).extract();
			if (LOOP.starts(tokens))
				return LOOP.load(tokens).extract();
			if (OPERATION.starts(tokens))
				return OPERATION.load(tokens).extract();
			return GROUP_REF.load(tokens).extract();
		}

		public void setFirsts() {
			firsts = new HashSet<Character>();
			firsts.addAll(INTERVAL.firsts());
			firsts.addAll(SET.firsts());
			firsts.addAll(SEQUENCE.firsts());
			firsts.addAll(LOOP.firsts());
			firsts.addAll(OPERATION.firsts());
		}
	},
	SEQUENCE_BODY {
		/*
		 * SEQUENCY_BODY -> REGEX_LIST | GROUP
		 */
		protected Object extract() { // XRegexNode
			if (REGEX_LIST.starts(tokens))
				return REGEX_LIST.load(tokens).extract();
			return GROUP.load(tokens).extract();
		}

		public void setFirsts() {
			firsts = new HashSet<Character>();
			firsts.addAll(REGEX_LIST.firsts());
			firsts.addAll(GROUP.firsts());
		}
	},
	OPERATION_TAIL {
		/*
		 * *OPERATION_TAIL -> CONDITION | SUBSTITUTION
		 */
		protected Object extract() { // XRegexNode
			if (CONDITION.starts(tokens))
				return CONDITION.load(tokens).extract();
			if (SUBSTITUTION.starts(tokens))
				return SUBSTITUTION.load(tokens).extract();
			return null;
		}

		public void setFirsts() {
			firsts = new HashSet<Character>();
			firsts.addAll(CONDITION.firsts());
			firsts.addAll(SUBSTITUTION.firsts());
		}
	},
	SEQUENCE {
		/*
		 * SEQUENCE -> DELIM_SEQ | SEQUENCE_GROUP
		 */
		protected Object extract() { // XRegexNode
			if (DELIM_SEQ.starts(tokens))
				return DELIM_SEQ.load(tokens).extract();
			return SEQUENCE_GROUP.load(tokens).extract();
		}

		public void setFirsts() {
			firsts = new HashSet<Character>();
			firsts.addAll(DELIM_SEQ.firsts());
			firsts.addAll(SEQUENCE_GROUP.firsts());
		}
	},
	SURROGATE {
		/*
		 * SURROGATE -> QUOTED_REGEX | FUNCT
		 */
		protected Object extract() { // XRegexNode
			if (QUOTED_REGEX.starts(tokens))
				return QUOTED_REGEX.load(tokens).extract();
			return FUNCT.load(tokens).extract();
		}

		public void setFirsts() {
			firsts = new HashSet<Character>();
			firsts.addAll(QUOTED_REGEX.firsts());
			firsts.addAll(FUNCT.firsts());
		}

	},
	
	// ///////////////////////////////////////////////////////////////////////////////////////////////////
	// EXECUTORS
	// ///////////////////////////////////////////////////////////////////////////////////////////////////

	REGEX {
		/*
		 * REGEX -> REGEX_BODY OCCURRENCES
		 */
		protected XRegexNode extract() {
			List<Object> param = new ArrayList<Object>();
			param.add(REGEX_BODY.load(tokens).extract());
			param.add(OCCURRENCES.load(tokens).extract());
			return new XRegexNode(this, param);
		}

		public void setFirsts() {
			firsts = REGEX_BODY.firsts();
		}
	},
	GROUP_REF {
		/*
		 * GROUP_REF -> '\' NUMBER
		 */
		protected XRegexNode extract() {
			consumes(tokens, "\\");
			List<Object> param = new ArrayList<Object>();
			param.add(NUMBER.load(tokens).extract());
			return new XRegexNode(this, param);
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('\\'));
		}
	},
	DECLARE {
		/*
		 * DECLARE -> ALPHA_STARTED_WITH_LETTER ':' REGEX
		 */
		protected Object extract() {
			if (ALPHA_STARTED_WITH_LETTER.starts(tokens)) {
				List<Object> param = new ArrayList<Object>();
				param.add(ALPHA_STARTED_WITH_LETTER.load(tokens).extract());
				consumes(tokens, ":");
				param.add(REGEX.load(tokens).extract());
				return new XRegexNode(this, param);
			}
			return REGEX.load(tokens).extract();
		}

		public void setFirsts() {
			firsts = ALPHA_STARTED_WITH_LETTER.firsts();
		}
	},
	LOOP {
		/*
		 * LOOP -> '|' REGEX_LIST '|<' LOOP_BODY
		 */
		protected Object extract() {
			consumes(tokens, "|");
			List<Object> param = new ArrayList<Object>();
			param.add(REGEX_LIST.load(tokens).extract());
			consumes(tokens, "|<");
			param.add(VAR_OCCURRENCES_COUNT.load(tokens).extract());
			return new XRegexNode(this, param);
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('|'));
		}
	},
	SET {
		/*
		 * SET -> EXCEPT '{' REGEX_LIST '}' SET_SEQ_TAIL COLISION
		 */
		protected Object extract() {
			List<Object> param = new ArrayList<Object>();
			Object xp = EXCEPT.load(tokens).extract();
			param.add(xp);
			consumes(tokens, "{");
			param.add(REGEX_LIST.load(tokens).extract());
			consumes(tokens, "}");
			param.add(SET_SEQ_TAIL.load(tokens).extract());
			return new XRegexNode(this, param);
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('{'));
		}
	},	
	OPERATION {
		/*
		 * OPERATION -> EXCEPT ALPHA_STARTED_WITH_LETTER OPERATION_TAIL
		 */
		protected Object extract() {
			List<Object> param = new ArrayList<Object>();
			Object xp = EXCEPT.load(tokens).extract();
			param.add(xp);
			param.add(ALPHA_STARTED_WITH_LETTER.load(tokens).extract());
			Object tail = OPERATION_TAIL.load(tokens).extract();
			if (tail != null)
				param.add(tail);
			return new XRegexNode(this, param);
		}

		public void setFirsts() {
			firsts = ALPHA_STARTED_WITH_LETTER.firsts();
		}
	},
	INTERVAL {
		/*
		 * INTERVAL -> EXCEPT '\'' ANY_CHAR_NOTQUOTE_NOTTRACE INTERVAL_TAIL '\''
		 */
		protected Object extract() {
			consumes(tokens, "'");
			List<Object> param = new ArrayList<Object>();
			Object xp = EXCEPT.load(tokens).extract();
			param.add(xp);
			param.add(ANY_CHAR_NOTQUOTE_NOTTRACE.load(tokens).extract());
			Object tail = INTERVAL_TAIL.load(tokens).extract();
			if (tail != null)
				param.add(tail);
			consumes(tokens, "'");
			return new XRegexNode(this, param);
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('\''));
		}
	},
	CONDITION {
		/*
		 * CONDITION -> CONDITION_IF CONDITIONAL_THEN
		 */
		@SuppressWarnings("unchecked")
		protected Object extract() {
			Map<String, Object> condMap = new HashMap<String, Object>();
			condMap.putAll((Map<String, Object>) CONDITION_IF.load(tokens).extract());
			condMap.putAll((Map<String, Object>) CONDITIONAL_THEN.load(tokens).extract());
			return new XRegexNode(this, Arrays.asList(condMap));
		}

		public void setFirsts() {
			firsts = CONDITION_IF.firsts();
		}
	},
	SUBSTITUTION {
		/*
		 * SUBSTITUTION -> '=>' SURROGATE
		 */
		protected Object extract() {
			consumes(tokens, "=>");
			List<Object> param = new ArrayList<Object>();
			param.add(QUOTED_REGEX.load(tokens).extract());
			return new XRegexNode(this, param);
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('='));
		}
	},
	DELIM_SEQ {
		/*
		 * DELIM_SEQ -> '^' REGEX DELIM_SEQ_BODY REGEX '$'
		 */
		protected Object extract() {
			List<Object> param = new ArrayList<Object>();
			consumes(tokens, "^");
			param.addAll((List<?>) REGEX_LIST.load(tokens).extract());
			consumes(tokens, "$");
			return new XRegexNode(this, param);
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('^'));
		}
	},
	SEQUENCE_GROUP {
		/*
		 * SEQUENCE_GROUP -> '[' SEQUENCY_BODY ']' SET_SEQ_TAIL COLISION
		 */
		protected Object extract() {
			List<Object> param = new ArrayList<Object>();
			consumes(tokens, "[");
			param.add(SEQUENCE_BODY.load(tokens).extract());
			consumes(tokens, "]");
			param.add(SET_SEQ_TAIL.load(tokens).extract());
			return new XRegexNode(this, param);
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList('['));
		}
	},
	GROUP {
		/*
		 * GROUP -> ':' REGEX_LIST ':'
		 */
		@SuppressWarnings("unchecked")
		protected Object extract() {
			consumes(tokens, ":");
			List<Object> param = (List<Object>) REGEX_LIST.load(tokens).extract();
			consumes(tokens, ":");
			return new XRegexNode(this, param);
		}

		public void setFirsts() {
			firsts = new HashSet<Character>(Arrays.asList(':'));
		}
	};

	/******************************************************************************************************/
	/******************************************************************************************************/
		
	protected Set<Character> firsts;
	protected Object tokens;
	
	protected abstract Object extract();
	protected abstract void setFirsts();

	public Object extract(String tokens) {
		this.tokens = new StringBuilder(StringUtil.cleanEscapesOutOfString(tokens));
		return extract();
	}

	public Object extract(List<Token> tokens) {
		this.tokens = tokens;
		return extract();
	}
	
	public List<XRegexLeaf> run(String text, String pattern) {
		Object extracted = extract(pattern);
		if (!(extracted instanceof XRegexNode))
			throw new RuntimeException("XRegex sem interface pública: " + this);
		return ((XRegexNode) extracted).run(text);
	}

	public XRegex load(Object tokens) {
		this.tokens = tokens;
		return this;
	}
	
	public Object tokens() {
		return tokens;
	}

	public Set<Character> firsts() {
		if (firsts == null)
			setFirsts();
		return firsts;
	}
}