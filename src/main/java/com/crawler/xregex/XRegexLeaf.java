package com.crawler.xregex;

import com.crawler.enums.TokenOption;
import com.crawler.tokenizer.Token;

public class XRegexLeaf {
	
	public Token token;
	public int start, end;

	public XRegexLeaf(String group, int start, int end) {
		this.token = new Token(group, TokenOption.NONE);
		this.start = start;
		this.end = end;
	}
	
	@Override
	public String toString() {
		return token + "";
	}
}