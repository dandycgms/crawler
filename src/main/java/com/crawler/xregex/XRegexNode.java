package com.crawler.xregex;

import java.util.List;

public class XRegexNode {
	
	public XRegexRunner id;
	public List<Object> param;
	
	public XRegexNode(XRegex rg, List<Object> param) {
		this.id = XRegexRunner.valueOf(rg.toString());
		this.param = param;
	}
	
	public List<XRegexLeaf> run(String string) {
		return id.load(param).run(string);
	}

	@Override
	public String toString() {
		return "XRegexNode [id=" + id + ", param=" + param + "]";
	}
}