package com.crawler.xregex;

import java.util.List;
import java.util.Set;

import com.crawler.tokenizer.Token;
import com.crawler.tokenizer.Tokenizer;

@SuppressWarnings("unchecked")
public interface AbstractXRegex {

	abstract Set<Character> firsts();
	
	default boolean signsearch() {
		return true;
	}

	default String firstConsumes(Object tokens) {
		if (starts(tokens)) 
			return advance(tokens).toString();
		throw new RuntimeException("Não deu matching no começo de " + this);
	}

	default boolean starts(Object tokens) {
		if (tokens instanceof StringBuilder)
			return startsString((StringBuilder)tokens);
		if (tokens instanceof String)
			return startsString(new StringBuilder((String) tokens));
		return startsList((List<Token>) tokens);
	}
	default String consumes(Object tokens, String string) {
		if (tokens instanceof StringBuilder)
			return consumesString((StringBuilder)tokens, string);
		if (tokens instanceof String)
			return consumesString(new StringBuilder((String) tokens), string);
		return consumesList((List<Token>)tokens, Tokenizer.splitInTokens(string));
	}
	default String advance(Object tokens) {
		if (tokens instanceof StringBuilder)
			return advanceString((StringBuilder)tokens);
		if (tokens instanceof String)
			return advanceString(new StringBuilder((String) tokens));
		return advanceList((List<Token>) tokens);
	}

	default boolean startsList(List<Token> tokens) {
		if (tokens.isEmpty())
			return false;
		if (signsearch())
			return firsts().contains(tokens.get(0).first());
		return !firsts().contains(tokens.get(0).first());
	}

	default boolean startsString(StringBuilder tokens) {
		if (tokens.length() == 0)
			return false;
		if (signsearch())
			return firsts().contains(tokens.charAt(0));
		return !firsts().contains(tokens.charAt(0));
	}

	default String consumesList(List<Token> tokens, List<Token>  pattern) {
		if (tokens.size() < pattern.size())
			throw new RuntimeException("O padrão de busca é maior que o próprio texto de entrada! Era esperado o token {" + pattern.get(0) + "}.");
		String s = "";
		for (int i = 0; i < pattern.size(); i++) {
			if (!tokens.get(i).equals(pattern.get(i)))
				throw new RuntimeException("Era esperado o token {" + pattern.get(i) + "} mas veio o token {" + tokens.get(i) + "}");
			s += advance(tokens);
		}
		return s;
	}
	default String consumesString(StringBuilder tokens, String pattern) {
		if (tokens.length() < pattern.length())
			throw new RuntimeException("O padrão de busca é maior que o próprio texto de entrada! Era esperado o token {" + pattern + "}.");
		if (!tokens.toString().startsWith(pattern))
			throw new RuntimeException("Era esperado o token {" + pattern + "} mas veio o token {" + tokens + "}");
		tokens.delete(0, pattern.length());
		return pattern;
	}

	default String advanceString(StringBuilder tokens) {
		String r = tokens.substring(0, 1);
		tokens.deleteCharAt(0);
		return r;
	}
	default String advanceList(List<Token> tokens) {
		return tokens.remove(0).toString();
	}
}
