package com.crawler.xregex;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum XRegexRunner {
	/****************************************************************************** 
	 * IDEIA GERAL: Buscar primeiro padrão no texto que dá um match.
	 *   Occurences: Especificar a quantidade caso for diferente disso
	 *   TODO Order: Em contraposição às ocorrências, aplica-se ao i-ésimo padrão (default 1º)
	 *   Lazy: Em um busca preguiçosa, busca-se (primeiro) melhor hit (especialmente em * ou +)
	 *   Exception: Negar com ~ padrões de SET, INTERVAL e OPERATION
	 *   Sparse: Dizer quando se deseja buscar um padrão esparsamente ou não
	 *   Insensitive-Case: Dizer se a busca é sensivel ao caso ou não
	 *   Colision: Dizer o quanto a busca admite ocorrências de colisões
	 *   Separator: Busca de padrões separados por algo a especificar
	 *   Variable: Registra-se uma variável inteira num loop ou numa XREGEX qualquer para uso posterior
	 *   Condition: Avalia uma função específica para saber se executa determinados XRegex
	 *   Function: Função de avaliação, que retorna um padrão a ser avaliado ou substituído
	 *   Substitution: Substitui as ocorrências de um padrão por um valor ou função
	 *   Group: Seta uma sequência como grupo a ser posteriormente referenciado
	 *   
	 *   TIPOS:
	 *   	SET: Conjunto. Occorências sem preservar ordem, com repetição 
	 *   	SEQUENCE: Sequência. Occorências preservando ordem, sem repetição
	 *   	TODO SORT: Arranjo. Occorências sem preservar ordem, sem repetição
	 *   	DELIM-SEQ: Delimitador de início e fim de sequência
	 *   	SEQUENCE_GROUP: Sequência-grupo: Define sequência ou grupo com sua respectiva tail 
	 *   	LOOP: Laço. Encontra certo padrão um número variável de vezes
	 *   	INTERVAL: intervalo. Encontra um padrão dado um certo intervalo de caracteres
	 *   	OPERATION: Operação. Avalia-se uma XRegex por condicional ou substituição
	 *   	GROUP-REF: Referência. Recupera uma XRegex agrupada avaliada anteriormente
	 *   	DECLARE: Declaração. Rotula uma XRegex
	 ******************************************************************************/
	INTERVAL {
		protected List<XRegexLeaf> run(String text) {
			if (param.size() == 2) {
				String pattern = (String) param.get(1);
				pattern = pattern.replace("\\@", "[A-Za-z0-9]");
				pattern = pattern.replace("\\w", "[a-z]");
				pattern = pattern.replace("\\W", "[A-Z]");
				pattern = pattern.replace("\\a", "[A-Za-z]");
				pattern = pattern.replace("\\d", "[0-9]");
				pattern = pattern.replace("\\p", "[\\u005C\\-!\"#$%&'()*+,\\./:;\\<=\\>?@\\[\\]_`{|}~]");
				pattern = pattern.replace("\\c", "[\\u0000-\\u001F\\u007F]");
				pattern = pattern.replace("\\v", "[\\u0020-\\u007E]");
				pattern = pattern.replace("\\s", "[ \t\r\n]");
				return printMatches(text, pattern, (boolean)param.get(0));
			}
			String patternFrom = (String) param.get(1);
			String patternTo = (String) param.get(2);
			if (isControlInterval(patternFrom))
				throw new RuntimeException("Em um intervalo fechado com inicio e fim não se pode ter caracter especial de conjunto: " + patternFrom);
			if (isControlInterval(patternTo))
				throw new RuntimeException("Em um intervalo fechado com inicio e fim não se pode ter caracter especial de conjunto: " + patternTo);
			if (patternFrom.length() != patternTo.length())
				throw new RuntimeException("Em um intervalo fechado, a sequência de início '" + patternFrom + "' deve ter o mesmo número de caracteres que a sequência de fim '" + patternTo + "'");
			String pattern = "";
			for (int i = 0; i < patternFrom.length(); i++) {
				char charFrom = patternFrom.charAt(i);
				char charTo = patternTo.charAt(i);
				if (charFrom > charTo)
					throw new RuntimeException("Em um intervalo fechado, o caractere de início '" + charFrom + "' deve ser lexicograficamente anterior ao caracter de fim '" + charTo + "'");
				pattern += "[" + charFrom + "-" + charTo + "]";
			}
			return printMatches(text, pattern, (boolean)param.get(0));
		}
		
		private boolean isControlInterval(String patternTo) {
			return  patternTo.contains("\\@")|
					patternTo.contains("\\w")|
					patternTo.contains("\\W")|
					patternTo.contains("\\a")|
					patternTo.contains("\\d")|
					patternTo.contains("\\p")|
					patternTo.contains("\\c")|
					patternTo.contains("\\v")|
					patternTo.contains("\\s");
		}
	},
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/

	GROUP_REF {
		protected List<XRegexLeaf> run(String string) {
			System.out.println("Loading " + this + ": " + param);
			return null;
		}
	},
	DECLARE {
		protected List<XRegexLeaf> run(String string) {
			//TODO Monta LookupTable(HashMap) que mapeia ID a REGEX
			System.out.println("Loading " + this + ": " + param);
			//String id = param.get(0).toString();
			List<XRegexLeaf> ret = ((XRegexNode) param.get(1)).run(string);
			return ret;
		}
	},
	LOOP {
		protected List<XRegexLeaf> run(String string) {
			System.out.println("Loading " + this + ": " + param);
			return null;
		}
	},
	SET {
		protected List<XRegexLeaf> run(String string) {
			System.out.println("Loading " + this + ": " + param);
			return null;
		}
	},
	OPERATION {
		protected List<XRegexLeaf> run(String string) {
			System.out.println("Loading " + this + ": " + param);
			return null;
		}
	},
	CONDITION {
		protected List<XRegexLeaf> run(String string) {
			System.out.println("Loading " + this + ": " + param);
			// apenas de reação
			return null;
		}
	},
	SUBSTITUTION {
		protected List<XRegexLeaf> run(String string) {
			System.out.println("Loading " + this + ": " + param);
			// apenas de reação
			return null;
		}
	},
	DELIM_SEQ {
		protected List<XRegexLeaf> run(String string) {
			System.out.println("Loading " + this + ": " + param);
			return null;
		}
	},
	SEQUENCE_GROUP {
		protected List<XRegexLeaf> run(String string) {
			System.out.println("Loading " + this + ": " + param);
			return null;
		}
	},
	GROUP {
		protected List<XRegexLeaf> run(String string) {
			System.out.println("Loading " + this + ": " + param);
			// apenas de reação
			return null;
		}
	},
	REGEX {
		protected List<XRegexLeaf> run(String string) {
			System.out.println("Loading " + this + ": " + param);
			List<XRegexLeaf> ret = ((XRegexNode) param.get(0)).run(string);
			// TODO rodar param.get(0) vezes
			return ret;
		}
	};

	protected abstract List<XRegexLeaf> run(String string);
	protected List<Object> param;
	
	public XRegexRunner load(List<Object> param) {
		this.param = param;
		return this;
	}
	
	public static List<XRegexLeaf> printMatches(String text, String regex) {
		return printMatches(text, regex, false);
	}
	
	public static List<XRegexLeaf> printMatches(String text, String regex, boolean direct) {
		List<XRegexLeaf> leaves = new ArrayList<XRegexLeaf>();
	    Pattern pattern = Pattern.compile(regex);
	    Matcher matcher = pattern.matcher(text);
	    while (matcher.find()) 
	        leaves.add(new XRegexLeaf(matcher.group(), matcher.start(),matcher.end()));
	    if (!direct)
	    	return negation(leaves, text);
	    return leaves;
	}

	private static List<XRegexLeaf> negation(List<XRegexLeaf> leaves, String text) {
		List<XRegexLeaf> ret = new ArrayList<XRegexLeaf>();
		int left = 0, right;
		for (XRegexLeaf leaf : leaves) {
			right = leaf.start;
			if (right > left)
				ret.add(new XRegexLeaf(text.substring(left,right), left, right));
			left = leaf.end;
		}
		right = text.length();
		if (right > left)
			ret.add(new XRegexLeaf(text.substring(left,right), left, right));
		return ret;
	}
}